from django.apps import apps
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms import formset_factory, inlineformset_factory, model_to_dict
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.views.generic import View

    
from sorl.thumbnail import get_thumbnail

from .forms import *
from .models import *
from .pdfgenerator import render_to_pdf

from datetime import datetime
import operator


class Home(LoginRequiredMixin, View):
    '''
    Home Page - Determines if to start a new study or continue on an existing one.
    '''
    template = 'survey/home.html'
    form_class = ExistingParticipantForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {'form': form}

        return render_to_response(self.template, context, context_instance=RequestContext(request))


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        error = ''
        participant_number = ''

        if form.is_valid():
            participant_number = form.cleaned_data['participant_number']

            return redirect('%s?sn=%s' % (reverse('gen-info'), participant_number))
        else:
            context = {'form': form, 'error': form.errors['participant_number'] }
            return render_to_response(self.template, context, context_instance=RequestContext(request))


class Login(View):
    '''
    Login Page
    '''
    template = 'survey/login.html'
    form_class = ExistingParticipantForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {'form': form}

        return render_to_response(self.template, context, context_instance=RequestContext(request))


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        error = ''
        participant_number = ''

        if form.is_valid():
            participant_number = form.cleaned_data['participant_number']

            return redirect('%s?sn=%s' % (reverse('gen-info'), participant_number))
        else:
            context = {'form': form, 'error': form.errors['participant_number'] }
            return render_to_response(self.template, context, context_instance=RequestContext(request))



class BaseView(LoginRequiredMixin, View):
    '''
    Base
    '''
    template = ''
    forms_cls = []
    formsets = []

    def get(self, request, *args, **kwargs):
        participant_number = request.GET.get('sn', None)

        # Indicator to create a new record
        start_new = request.GET.get('start_new', None)
        participant = None
        context = {}

        # Either to retrieve or create a new record
        if participant_number:
            participant, created = Participant.objects.get_or_create(participant_number=participant_number)
        elif start_new:
            participant = Participant(participant_number=generate_participant_number(), date_started=datetime.now())
            participant.save()
        else:
            return redirect('home')
        context.update({'participant': participant})

        forms_cls = self.forms_cls
        if forms_cls:
            for form_item in forms_cls:
                form_instance = form_item[0](instance=participant)
                context.update({form_item[1]: form_instance})

        formsets = self.formsets
        if formsets:
            for formset in formsets:
                formset_class = inlineformset_factory(Participant, formset[0], form=formset[1], extra=1, can_delete=True)
                formset_instance = formset_class(instance=participant)
                context.update({formset[2]:formset_instance})

        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        participant_number = request.POST.get('participant_number')
        participants = Participant.objects.filter(participant_number=participant_number)
        success = True
        success_message = ''
        context = {}

        participant = None
        if 'participant_number' in request.POST and participants.exists():
            participant = participants[0]

        forms_cls = self.forms_cls
        if forms_cls:
            for form_item in forms_cls:
                form_instance = form_item[0](request.POST, instance=participant)
                context.update({form_item[1]: form_instance})

                if form_instance.is_valid():
                    form_instance.save()
                    success = success and True
                else:
                    success = False

        formsets = self.formsets
        if formsets:
            for formset in formsets:
                formset_class = inlineformset_factory(Participant, formset[0], form=formset[1], extra=1, can_delete=True)
                formset_instance = formset_class(request.POST, instance=participant)
                context.update({formset[2]:formset_instance})

                if formset_instance.is_valid():
                    formset_instance.save()
                    formset_instance = formset_class(instance=participant)
                    success = success and True
                else:
                    success = False
                    break
                context.update({formset[2]:formset_instance})

        if success:
            success_message = 'Save successful!'
        context.update({'success_message': success_message, 'participant': participant})
        return render_to_response(self.template, context, context_instance=RequestContext(request))


class GenInfo(BaseView):
    '''
    General Information Page
    '''
    template = 'survey/gen_info.html'
    forms_cls = [ (GeneralInfoForm, 'form' ), (GenInfo_HouseholdForm, 'form2')]


class HealthLifestyle(BaseView):
    '''
    Health & Lifestyle
    '''
    template = 'survey/health_lifestyle.html'
    forms_cls = [ (HealthLifestyleForm, 'form' ),
            (HealthLifestyleForm2, 'form2'),
            (SunlightExposureForm, 'form3'),
            (HealthLifestyleForm3, 'form4'),
            (NearWorkForm, 'form5'),
            (HealthLifestyleForm4, 'form6'),
            (HealthLifestyleForm5, 'form7'),
            (Location_HouseholdForm, 'form8'),
            (HealthLifestyleForm6, 'form9')
        ]
    formsets = [(SystemicDisease, SystemicDiseaseForm, 'systemic_disease_formset'), 
            (LifeStyleCondition, LifeStyleConditionForm, 'lifestyle_condition_formset'),
            (SystemicMedication, SystemicMedicationForm, 'systemic_medication_formset'),
            (OcularMedication, OcularMedicationForm, 'ocular_medication_formset'),
            (DietarySupplement, DietarySupplementForm, 'dietary_supplement_formset'),
            (AlcoholicBeverage, AlcoholicBeverageForm, 'alcoholic_beverages_formset'),
        ]


class SocioEconomic(BaseView):
    '''
    SocioEconomic
    '''
    template = 'survey/socio_economic.html'
    forms_cls = [ (SocioEconomicForm, 'form' ),]


class EyeExam(BaseView):
    '''
    EyeExam
    '''
    template = 'survey/eye_exam.html'
    forms_cls = [
            (VisualAcuityForm, 'form'),
            (AutomatedRefractionForm, 'form2'),
            (EyeGlassesForm, 'form3'),
            (TonometryDilationGlusoseLevelBPForm, 'form4'),
            (IshiharaForm, 'form5'),
        ]
    formsets = [
            (SystemicDisease, SystemicDiseaseForm, 'formset'),
            (LifeStyleCondition, LifeStyleConditionForm, 'formset2'),
            (SystemicMedication, SystemicMedicationForm, 'formset3'),
            (OcularMedication, OcularMedicationForm, 'formset4'),
            (OcularSymptom, OcularSymptomForm, 'formset5'),
            (NonOcularSymptom, NonOcularSymptomForm, 'formset6'),
            (OcularFindingDiagnosis, OcularFindingDiagnosisForm, 'formset7'),
            (OcularProcedure, OcularProcedureForm, 'formset8'),
            (Management, ManagementForm, 'formset9'),
        ]


class Diagnostics(BaseView):
    '''
    Diagnostics
    '''
    template = 'survey/diagnostics.html'
    forms_cls = []


class PictureUpload(View):
    
    def post(self, request, *args, **kwargs):
        response = ''
        try:
            participants = Participant.objects.filter(participant_number=request.POST.get('pn'))
            field_to_update = request.POST.get('field')
            width = request.POST.get('width')
            height = request.POST.get('height')

            participant = None
            if participants.exists():
                participant = participants[0]
            else:
                participant = Participant(participant_number=request.POST.get('pn'))
            setattr(participant, field_to_update, request.FILES.get('uploaded-image'))
            participant.save()

            image = getattr(participant, field_to_update)
            thumb = get_thumbnail(image, '%sx%s' % (width, height))
            response = thumb.url
        except Exception as e:
            response = str(e)

        return HttpResponse(response)

class HouseholdGenerator(View):

    def get(self, request, *args, **kwargs):
        DEFAULT = 1001
        response = DEFAULT
        latest_household = Household.objects.all().order_by('-code')[:1]

        if latest_household:
            new_code = latest_household[0].code + 1
            Household(code=new_code).save()
            response = new_code

        return HttpResponse(response)


class HouseholdRetriever(View):

    def get(self, request, *args, **kwargs):
        code = request.GET.get('code')
        fields = request.GET.get('fields').split(',')
        household = get_object_or_404(Household, code=code)

        household_dict = model_to_dict(household, fields)

        return JsonResponse(household_dict)


class DropdownDetails_Analytics(View):

    def get(self, request, *args, **kwargs):
        dropdown_id = request.GET.get('dropdown_id')
        dropdown = get_object_or_404(Dropdown, id=dropdown_id)

        model_class = apps.get_model(app_label='survey', model_name=dropdown.model_name)
        field = model_class._meta.get_field(dropdown.field_name)

        options = [ option[1] for option in dropdown.get_options()]
        response = {
            'type': field.__class__.__name__,
            'options': options,
        }

        return JsonResponse(response)


class DropdownListing(View):
    template = 'survey/dropdown.html'

    def get(self, request, *args, **kwargs):
        context = {}
        dropdown_id = request.GET.get('dropdown_id')
        dropdown_instance = None

        if dropdown_id:
            dropdown_instance = get_object_or_404(Dropdown, id=dropdown_id)
        else:
            dropdown_list = Dropdown.objects.all().order_by('display_name')[:1]

            # if one exists
            if dropdown_list.exists():
                dropdown_instance = dropdown_list[0]

        # Selection form and add multiple options form
        form = DropdownListingForm()
        form.fields['variables'].initial = dropdown_instance
        context.update({'form': form})

        form2 = DropdownTabForm(instance=dropdown_instance)
        context.update({'form2': form2})

        # Dropdown option formsets
        formset_class = inlineformset_factory(Dropdown, DropdownOption, form=DropdownOptionForm, extra=0, can_delete=True)
        formset = formset_class(instance=dropdown_instance)
        context.update({'formset': formset})

        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        context = {}
        dropdown_instance = None

        form = DropdownListingForm(request.POST)
        formset = None
        if form.is_valid():
            dropdown_instance = form.cleaned_data['variables']

            form2 = DropdownTabForm(request.POST, instance=dropdown_instance)
            if form2.is_valid():
                form2.save()
                context.update({'form2': form2})

            if form.cleaned_data['add_multiple_options']:
                options = form.cleaned_data['add_multiple_options'].split('\n')

                DropdownOption.objects.bulk_create([ DropdownOption(dropdown=dropdown_instance, value=val.strip())
                                                    for val in options ])

                form = DropdownListingForm()
                form.fields['variables'].initial = dropdown_instance

            formset_class = inlineformset_factory(Dropdown, DropdownOption, form=DropdownOptionForm, extra=0, can_delete=True)
            formset = formset_class(request.POST, instance=dropdown_instance)

            if formset.is_valid():
                for dropdownOption in formset:
                    if dropdownOption.cleaned_data['DELETE']:
                        value = dropdownOption.cleaned_data['value']
                        replacement = dropdownOption.cleaned_data['replacement']
                        model_class = apps.get_model(app_label='survey', model_name=dropdown_instance.model_name)
                        model_class.objects.filter(**{dropdown_instance.field_name: value}).update(**{dropdown_instance.field_name: replacement})

                formset.save()
                formset = formset_class(instance=dropdown_instance)
                context.update({'success_message': 'Save Successful!'})
        else:
            context.update({'error_message': form.errors })
        context.update({'form': form})
        context.update({'formset': formset})

        return render_to_response(self.template, context, context_instance=RequestContext(request))


class Analysis(View):
    '''
    Analysis Tab
    '''
    template = 'survey/analysis.html'

    def get(self, request, *args, **kwargs):
        formset = formset_factory(ConditionForm, extra=1, can_delete=True)()
        context = {'formset': formset}

        return render_to_response(self.template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        formset_class = formset_factory(ConditionForm, extra=1, can_delete=True, can_order=True)
        formset = formset_class(request.POST)
        error_message = ''
        results = []

        terms = []
        coeffs = []
        if formset.is_valid():
            for form in formset:
                cleaned_data = form.cleaned_data
                field_name = ''

                if 'variable' in cleaned_data and cleaned_data['variable'] and cleaned_data['value'] and not cleaned_data['DELETE']:
                    partial_q_string = self.get_Q_string_representation(
                        cleaned_data['variable'].model_name,
                        cleaned_data['variable'].field_name,
                        cleaned_data['operation']
                    )
                    if cleaned_data['operation'] == '!=':
                        coeffs.append(~Q((partial_q_string, cleaned_data['value'])))
                    else:
                        coeffs.append(Q((partial_q_string, cleaned_data['value'])))

                    if cleaned_data['conjunction'] == 'OR':
                        terms.append(reduce(operator.and_, coeffs))
                        coeffs = []

            if coeffs:
                terms.append(reduce(operator.and_, coeffs))
            if terms:
                try:
                    results = Participant.objects.filter(reduce(operator.or_, terms))
                except Exception, e:
                    pass

        paginator = Paginator(results, 10)
        page = request.POST.get('page')
        try:
            paginated_results = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            paginated_results = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            paginated_results = paginator.page(paginator.num_pages)

        context = {
            'formset': formset,
            'error_message': error_message,
            'results': paginated_results,
        }

        return render_to_response(self.template, context, context_instance=RequestContext(request))


    def get_Q_string_representation(self, model_name, field_name, operation):
        
        if model_name == 'Participant':
            q_string = ''
        elif model_name == 'Household':
            q_string = 'household_code__'
        else:
            q_string = model_name.lower() + '__'

        q_string += field_name

        if operation == '>':
            q_string += '__gt'
        elif operation == '>=':
            q_string += '__gte'
        elif operation == '=':
            q_string += ''
        elif operation == '<':
            q_string += '__lt'
        elif operation == '<=':
            q_string += '__lte'
        elif operation == '!=':
            q_string += ''
        elif operation == 'CONTAINS':
            q_string += '__icontains'

        return q_string


class Notes(BaseView):
    forms_cls = [
        (NotesForm, 'form'),
    ]
    template = 'survey/notes.html'


def generate_participant_number():
    DEFAULT = 10001
    result = DEFAULT
    latest_participant = Participant.objects.all().order_by('-participant_number')[:1]

    if latest_participant:
        result = latest_participant[0].participant_number + 1

    return result


class DropdownDetails(View):

    def get(self, request, *args, **kwargs):
        dropdown_id = request.GET.get('id')
        dropdown_options = []

        dropdown_options += [{'id':opt.id, 'value':opt.value} for opt in Dropdown.objects.prefetch_related('dropdown_options').get(id=dropdown_id).dropdown_options.all()]

        return JsonResponse(dropdown_options, safe=False)


class Summary(View):
    pdf_template = "survey/printable.html"
    template = "survey/summary.html"


    def get(self, request, *args, **kwargs):
        context = {'pagesize':'A4'}

        participant_number = request.GET.get('sn', None)
        printable = request.GET.get('printable', None) == '1'

        if participant_number == '':
            return redirect('home')

        participant = get_object_or_404(Participant, participant_number=participant_number)
        context.update({'participant': participant})

        proportion_fields = [
            'rice_plate_proportion',
            'beef_plate_proportion',
            'pork_plate_proportion',
            'chicken_plate_proportion',
            'egg_plate_proportion',
            'fish_plate_proportion',
            'vegetables_plate_proportion',
            'junkfoods_plate_proportion',
        ]

        proportion_values = []
        for field in proportion_fields:
            val = getattr(participant, field)
            if val is not None and val != '':
                proportion_values.append('%s %s' % (val, field.replace('_plate_proportion', '')))

        participant.plate_proportions = ', '.join(proportion_values)

        for ocular_symptom in participant.ocularsymptom_set.filter(
                Q(main_symptom_od=True)|Q(main_symptom_os=True)|Q(main_symptom_patient=True)):
            if ocular_symptom.main_symptom_od:
                participant.main_symptom_od = ocular_symptom
            if ocular_symptom.main_symptom_os:
                participant.main_symptom_os = ocular_symptom
            if ocular_symptom.main_symptom_patient:
                participant.main_symptom_patient = ocular_symptom


        for ocular_diagnosis in participant.ocularfindingdiagnosis_set.filter(
                Q(main_diagnosis_od=True)|Q(main_diagnosis_os=True)|
                Q(main_diagnosis_patient=True)|Q(main_cause_visual_loss=True)):
            if ocular_diagnosis.main_diagnosis_od:
                participant.main_diagnosis_od = ocular_diagnosis
            if ocular_diagnosis.main_diagnosis_os:
                participant.main_diagnosis_os = ocular_diagnosis
            if ocular_diagnosis.main_diagnosis_patient:
                participant.main_diagnosis_patient = ocular_diagnosis
            if ocular_diagnosis.main_cause_visual_loss:
                participant.main_cause_visual_loss = ocular_diagnosis

        if printable:
            return render_to_pdf(
                self.pdf_template,
                context
            )
        else:
            return render_to_response(self.template, context, context_instance=RequestContext(request))


class HouseholdHeadUpdate(View):
    
    def post(self, request, *args, **kwargs):
        response = ''
        participant = get_object_or_404(Participant, participant_number=request.POST.get('pn'))
        participant.is_household_head = request.POST.get('is_household_head') == 'true'
        participant.save()

        return HttpResponse(response)


class FieldSave(View):

    def post(self, request, *args, **kwargs):
        response = ''
        participant = get_object_or_404(Participant, participant_number=request.POST.get('pn'))

        for field in request.POST:
            if field != 'pn' and field != 'csrfmiddlewaretoken':
                setattr(participant, field, eval(request.POST.get(field)))
        participant.save()

        return HttpResponse(response)


class FieldQuery(View):

    def get(self, request, *args, **kwargs):
        response = ''
        participant = get_object_or_404(Participant, participant_number=request.GET.get('pn'))
        field_name = request.GET.get('field_name')
        response = getattr(participant, field_name)

        return HttpResponse(response)


class TonometryDilationStatus(View):

    def get(self, request, *args, **kwargs):
        response = {}
        participant_number = request.GET.get('pn')
        if participant_number and participant_number != 'undefined':
            participant = get_object_or_404(Participant, participant_number=participant_number)
            response = {
                'for_tonometry': participant.for_tonometry,
                'for_tonometry_eye': participant.for_tonometry_eye,
                'for_tonometry_consented': participant.for_tonometry_consented,
                'for_dilation': participant.for_dilation,
                'for_dilation_eye': participant.for_dilation_eye,
                'for_dilation_consented': participant.for_dilation_consented,
            }

        return JsonResponse(response)