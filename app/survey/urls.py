from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.views.generic import TemplateView

from .views import *


urlpatterns = [
	url(r'^$', Home.as_view(), name='home'),
	url(r'gen-info/$', GenInfo.as_view(), name='gen-info'),
    url(r'summary/$', Summary.as_view(), name='summary'),
	url(r'health-lifestyle/$', HealthLifestyle.as_view(), name='health-lifestyle'),
	url(r'socio-economic/$', SocioEconomic.as_view(), name='socio-economic'),
	url(r'eye-exam/$', EyeExam.as_view(), name='eye-exam'),
	url(r'diagnostics/$', Diagnostics.as_view(), name='diagnostics'),
	url(r'picture-upload/$', PictureUpload.as_view(), name='picture-upload'),
	url(r'household-update/$', HouseholdHeadUpdate.as_view(), name="household-update"),
	url(r'household-generator/$', HouseholdGenerator.as_view(), name='household-generator'),
	url(r'household-retriever/$', HouseholdRetriever.as_view(), name='household-retriever'),
	url(r'dropdown-listing/$', DropdownListing.as_view(), name='dropdown-listing'),
	url(r'dropdown-details/$', DropdownDetails.as_view(), name='dropdown-details'),
	url(r'dropdown-details-analytics/$', DropdownDetails_Analytics.as_view(), name='dropdown-details-analytics'),
	url(r'field-save/$', FieldSave.as_view(), name='field-save'),
	url(r'field-query/$', FieldQuery.as_view(), name='field-query'),
	url(r'tonometry-dilation-status/$', TonometryDilationStatus.as_view(), name='tonometry-dilation-status'),
	url(r'analysis/$', Analysis.as_view(), name='analysis'),
	url(r'notes/$', Notes.as_view(), name='notes'),
	url(r'logout/$', logout, {'next_page': '/'}, name="logout"),
	url(r'login/$', login, {'template_name': 'survey/login.html'}, name='login'),
]	


