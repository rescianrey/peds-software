# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-05-26 15:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0138_auto_20160526_1523'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='alcoholicbeverage',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='dietarysupplement',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='management',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='ocularfindingdiagnosis',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='ocularmedication',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='ocularprocedure',
            options={'ordering': ['id']},
        ),
        migrations.AlterModelOptions(
            name='ocularsymptom',
            options={'ordering': ['id']},
        ),
    ]
