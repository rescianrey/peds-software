function showDivOnlyIf(divID, fieldId, value){
	showDivIfFieldEquals(divID, fieldId, value);
	$('#' + fieldId).change(function(){
		showDivIfFieldEquals(divID, fieldId, value);
	});
}

function syncFields(masterField, dependentField){
	$('#' + masterField).change(function(){
		console.log($(this).val());
		setValue($('#' + dependentField), $(this).val());

	});
}

function showDivIfFieldEquals(divID, fieldId, value){
	var matched = false;
	if($('#' + fieldId).is('[type="checkbox"]')){
		if($('#' + fieldId).is(':checked') == value){
			matched = true;
		}
	}else{
		if(value.constructor === Array){
			var fieldValue = $('#' + fieldId).val();
			for(var i = 0; i < value.length; i++) {
		        if(fieldValue == value[i]){
		        	matched = true;
		        	break;
		        }
		    }
		}else{
			if($('#' + fieldId).val() == value){
				matched = true;
			}
		}
	}

	if(matched){
    	$('#' + divID).fadeIn();
    }else{
    	$('#' + divID).fadeOut();

    	resetInputFields('#' + divID);
    }
}

function canAddRow(tableID, override_delete){
	var table = $('#' + tableID);
	table.css('margin-bottom', '0px');
	table.after('<button class="btn btn-primary" id="add-row-'+ tableID +'" style="float: right; margin-bottom:20px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>');

	$('button#add-row-' + tableID).click(function(){
		addRow(tableID);
		return false;
	});
	
	if(override_delete){
		$('input[id$="DELETE"]', table).each(function(){
			if($(this).attr('checked')){
				$(this).parents('tr').hide();
			}
		});

		$('tbody tr', table).each(function(){
			var deleteBtn = $('<button class="btn btn-danger"></button');
			deleteBtn.append('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
			var td = $('<td class="delete-btn-cont"></td>');
			td.append(deleteBtn);
			$(this).append(td);
			td.show();
			var tr = $(this);
			deleteBtn.click(function(){
				deleteFunction(table, tr);
				return false;
			});
		});
		var lastRow = $('tbody tr:last', table);
		var rowClone = lastRow.clone();
		$(':input', rowClone).each(function(){
			$(this).addClass('for-cloning');

			if($(this).attr('id')){
				var oldId = $(this).attr('id');
				var newId = oldId.replace(/-\d+-/, '-1000-');
				$(this).attr('id', newId);

				var oldName = $(this).attr('name');
				var newName = oldName.replace(/-\d+-/, '-1000-');
				$(this).attr('name', newName);
			}
		});
		
		rowClone.hide();
		lastRow.after(rowClone);

		hideDeleteColumn(tableID);
	}
}

function resetInputFields(row){
	$(':input:not([name$="_FORMS"])', row).each(function(){
		if($(this).attr('type') == 'checkbox'){
			$(this).attr('checked', false);
		}else{
			setValue($(this), '');
		}
		$(this).trigger('change');
	});
}

function deleteFunction(table, row) {
	if($('tbody tr:visible', table).length == 1){
		resetInputFields(row);
	}else{
		$('input[id$="DELETE"]', row).attr('checked', true);					
		row.hide();
	}
}

function addRow(tableID){
	var table = $('#' + tableID);

	var formProperties = $('#'+tableID).prev();
	var totalForms = parseInt($('input[id$="TOTAL_FORMS"]', formProperties).val()) + 1;
	$('input[id$="TOTAL_FORMS"]', formProperties).val(totalForms);

	var row = $('tbody tr:last', table).clone();
	row.show();
	$(':input', row).each(function(){
		if($(this).attr('id')){
			var oldId = $(this).attr('id');
			var newId = oldId.replace(/-\d+-/, '-'+(totalForms - 1)+'-');
			$(this).attr('id', newId);

			var oldName = $(this).attr('name');
			var newName = oldName.replace(/-\d+-/, '-'+(totalForms - 1)+'-');
			$(this).attr('name', newName);
		}
		$(this).removeClass('for-cloning');
	});

	resetInputFields(row);
	initialize(row);

	// oh...
	if(table.attr('id') == 'conditions-table'){
		filterAssistant(row);
	}

	$('tbody tr:last', table).before(row);

	$('.delete-btn-cont button', row).click(function(){
		deleteFunction(table, row);
		return false;
	});
}

function hideDeleteColumn(tableID){
	var table = $('#' + tableID);
	$('th[for$="-DELETE"]', table).hide();
	$('th[id$="-DELETE"]', table).hide();
	$('td[id$="-DELETE"]', table).hide();
}


function makeImagesUploadable(validator){
	$('.image-uploadable').each(function(){
		var img = $(this);
		var uploadInput = $('<input type="file" style="display: none;" name="uploaded-image"></input>');
		
		img.after(uploadInput);
		img.click(function(){
			if(validator()){
				uploadInput.click();
			}
		});


		uploadInput.change(function(){
			var form = img.parents('form');
			var parentEls = img.parents().map(function() {
			    return this.tagName;
			  })
			  .get()
			  .join( ", " );
		  	console.log(parentEls);
			var formData = new FormData(form[0]);

			img.attr('src', spinner);
			$.ajax({
	            url: form.attr('action'),
	            type: form.attr('method'),
	            cache: false,
	            data: formData,
	            processData: false,
    			contentType: false,
	            success: function(data, textStatus, jqXHR){
					if(textStatus == 'success'){
						img.attr('src', data);
					}else{
						alert(data);
					}
				}
	        });

		});
	});
}

function initialize(dom){
	// Transform all date input types to date widgets
    $('.dateinput:not([readonly])', dom).attr('type', 'date');
    $('.dateinput input:not([readonly])', dom).attr('type', 'date');

    $('.monthpicker', dom).datepicker( {
        format: "mm/yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });

    $('input.monthpicker', dom).bind('changeDate change', function(){
        if($(this).val() != null){
            var out = $('input[id$="duration_auto_calc"]', $(this).parents('tr'));
            var element = $(this);
            setTimeout(function(){calculateMonths(element, out)}, 500);
        }
    });
    
    $('.can-add-row', dom).each(function(){
        canAddRow($('table', this).attr('id'), true);
    });

    // Selectize here
    $('select:not(.for-cloning)', dom).selectize({selectOnTab: true, total: 1500});

    $('[class^="checkbox-group"]', dom).each(function(){
        checkOnlyOne($(this));
    });

    $(':input[id^="id_alcoholicbeverage"]', dom).change(function(){
    	var elementID = $(this).attr('id');
    	var itemNumber = /id_alcoholicbeverage_set-(\d+)-.*/.exec(elementID)[1];
    	calculateAlcoholTotalGlasses(itemNumber);
    });
}

function monthDiff(d1, d2) {
    var biggerDate = null;
    var smallerDate = null;
    var monthDiff = null;

    if(d2 >= d1){
        biggerDate = d2;
        smallerDate = d1;
    }else{
        biggerDate = d1;
        smallerDate = d2;
    }

    if(biggerDate.getYear() == smallerDate.getYear()){
        monthDiff = biggerDate.getMonth() - smallerDate.getMonth();
    }else{
        if(smallerDate.getMonth() > biggerDate.getMonth()){
            monthDiff = (biggerDate.getYear() - smallerDate.getYear() - 1) * 12 + (12 - smallerDate.getMonth()) + biggerDate.getMonth();
        }else{
            monthDiff = biggerDate.getMonth() - smallerDate.getMonth() + (biggerDate.getYear() - smallerDate.getYear()) * 12;
        }
    }

    return monthDiff;
}

function calculateMonths(fieldSource, fieldOutput){
    var dateParts = fieldSource.val().split('/');
    var dateFrom = new Date(dateParts[1], parseInt(dateParts[0]) - 1, 1, 0, 0, 0, 0);
    var dateTo = new Date();
    fieldOutput.val(monthDiff(dateFrom, dateTo));
}

function getFieldValue(pn, field_name, callback){
	$.get(FIELD_QUERY_URL + '?pn=' + pn + '&field_name=' + field_name, function(data, textStatus, jqXHR){
    	callback(data);
    });
}

function calculateTotal(fieldSources, multiplierSources, divisorSource, fieldOutput){
	var sum_or_average = 0;
	for(i in fieldSources){
		if(multiplierSources != null){
			if(fieldSources[i].val() && multiplierSources[i].val()){
				sum_or_average += parseInt(fieldSources[i].val()) * parseInt(multiplierSources[i].val());
			}
		}else{
			if(fieldSources[i].val()){
				sum_or_average += parseInt(fieldSources[i].val());
			}
		}
	}


	if(divisorSource != null && divisorSource.val() != null){
		sum_or_average = Math.round((sum_or_average / parseInt(divisorSource.val())) * 100) / 100;
	}

    fieldOutput.val(sum_or_average);
}

function checkOnlyOne(element){
    var myRegexp = /(checkbox-group-\d+)/g;
    var match = myRegexp.exec(element.attr('class'));
    var checkboxGroup = match[1];
    element.change(function(){
        console.log($(this).is(':checked'));
        if($(this).is(':checked')){
            var elementID = $(this).attr('id');
            $('.'+checkboxGroup).each(function(){
                if($(this).attr('id') != elementID){
                    $(this).attr('checked', false);
                }
            });
        }
    });
}

function saveOnChange(modelFieldNames, value_functions, participantId, csrfToken){
	var getData = function(){
		var data = {};
		for(var i in modelFieldNames){
			data[modelFieldNames[i]] = value_functions[i]($('#id_' + modelFieldNames[i]));
		}
		return data;
	}
	for(var i in modelFieldNames){
		$('#id_' + modelFieldNames[i]).change(function(){
			$(this).attr('disabled', '');
			var data = getData();
			data['pn'] =  participantId;
			data['csrfmiddlewaretoken'] = csrfToken;

			var that = $(this);
			$.post(FIELD_SAVE_URL, data, function(){
				that.removeAttr('disabled');
			});
		});
	}
}

function calculateAlcoholTotalGlasses(itemNumber){
    var numberOfGlasses = $('#id_alcoholicbeverage_set-'+ itemNumber +'-number_of_glasses').val();
    var frequency = $('#id_alcoholicbeverage_set-'+ itemNumber +'-drinking_frequency').val();
    var duration = $('#id_alcoholicbeverage_set-'+ itemNumber +'-drinking_duration').val();

    if(numberOfGlasses != '' && frequency != '' && duration != ''){
        var equivalent_freq = 0;
        if(frequency == 'Everyday'){
            equivalent_freq = 365;
        }else if(frequency == '5-6 days a week'){
            equivalent_freq = 286;
        }else if(frequency == '3-4 days a week'){
            equivalent_freq = 182;
        }else if(frequency == '1-2 days a week'){
            equivalent_freq = 78;
        }else if(frequency == '2-3 days a month'){
            equivalent_freq = 30;
        }else if(frequency == '1 day a month'){
            equivalent_freq = 12;
        }else if(frequency == '6-11 days a year'){
            equivalent_freq = 9;
        }else if(frequency == 'Less than 6 days a year'){
            equivalent_freq = 3;
        }
        var total = numberOfGlasses * equivalent_freq * duration;
        $('#id_alcoholicbeverage_set-'+ itemNumber +'-drinking_total_glasses').val(total);

        var overall_total = 0;
        $('input[id$="drinking_total_glasses"]').each(function(){
        	overall_total += Number($(this).val());
        });
        $('#id_alcohol_total_glasses').val(overall_total);
    }
}

function setValue(jqElement, value){
	jqElement.val(value);
	if(jqElement[0].selectize){
		jqElement[0].selectize.setValue(value);
	}
}

var NUMBER_OPS = ['>', '>=', '=', '!=', '<', '<='];
var STRING_OPSN = ['=', '!=', 'CONTAINS'];


function filterAssistant(row){
	$('select[name$="variable"]', row).change(function(){
		var table_row = row;
		if($(this).val()){
			$.get(DROPDOWN_DETAILS_ANALYTICS_URL + '?dropdown_id=' + $(this).val(), function(data, status, xhr){
				var operation = $('select[name$="operation"]', table_row)[0].selectize;
				operation.clearOptions();
				console.log(data['type']);	
				if(data['type'] == 'CharField'){
					for(var i in STRING_OPSN){
						operation.addOption({value:STRING_OPSN[i], text:STRING_OPSN[i]});
					}
				}else{
					for(var i in NUMBER_OPS){
						operation.addOption({value:NUMBER_OPS[i], text:NUMBER_OPS[i]});
					}
				}
				console.log(data['options']);
				var value_input = $(':input[name$="value"]', table_row);
				if(data['options'].length > 1){
					var select;
					if(value_input[0].selectize){
						select = value_input;
						select[0].selectize.clearOptions();
					}else{
						select = $('<select>');
						select.addClass(value_input.attr('class'));
						select.attr('name', value_input.attr('name'));
						select.attr('id', value_input.attr('id'));
						value_input.after(select);
						value_input.remove();
						select.selectize();
					}

					var options = data['options'];
					for(var i in options){
						select[0].selectize.addOption({value:options[i], text:options[i]});
					}
				}else{
					console.log('erer');
					if(value_input[0].selectize){
						input = $('<input>');
						input.addClass(value_input.attr('class'));
						input.attr('name', value_input.attr('name'));
						input.attr('id', value_input.attr('id'));
						var container = value_input.parent();
						container.html('');
						container.append(input);
					}
				}
			}, 'json');
		}
	});
}