$(function() {
    $('#id_variables').change(function(){
        if($(this).val() != ''){
            var url = dropdownURL + '?dropdown_id=' + $(this).val();
            window.location = url;
        }
    });

    $('#submit').click(function(){
    	$('#dropdown_form').submit();
    });

    $('[id^="div_id_dropdown_options"][id$="-replacement"] input').attr('disabled', '');
    $('[id^="div_id_dropdown_options"][id$="-replacement"] select').attr('disabled', '');

    $('[id^="div_id_dropdown_options"][id$="-DELETE"] input').each(function(){
    	$(this).change(function(){
    		var elementID = $(this).attr('id');
    		var idRegex = /id_dropdown_options-(\d+)-DELETE/g;
    		var itemId = idRegex.exec(elementID)[1];
    		if($(this).is(':checked')){
    			$('[id^="div_id_dropdown_options-'+ itemId +'-replacement"] select').selectize()[0].selectize.enable();
    		}else{
    			$('[id^="div_id_dropdown_options-'+ itemId +'-replacement"] select').selectize()[0].selectize.disable();
    		}
    	});
    });
});
