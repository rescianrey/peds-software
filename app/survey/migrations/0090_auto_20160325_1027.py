# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-25 10:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0089_auto_20160325_1020'),
    ]

    operations = [
        migrations.RenameField(
            model_name='participant',
            old_name='HHEAD_occupational_status',
            new_name='household_head_occupational_status',
        ),
        migrations.RenameField(
            model_name='participant',
            old_name='HHEAD_work_sector',
            new_name='household_head_work_sector',
        ),
        migrations.RenameField(
            model_name='participant',
            old_name='hw_employment_status',
            new_name='housewife_employment_status',
        ),
    ]
