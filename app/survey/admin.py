from django.contrib import admin

from .models import *
from .forms import *

admin.site.register(Participant)


class DropdownOptionInline(admin.StackedInline):
    model = DropdownOption
    extra = 1

class DropdownAdmin(admin.ModelAdmin):
    #inlines = [ DropdownOptionInline ]
    form = DropdownForm
    search_fields = ['field_name','model_name', 'display_name']

admin.site.register(Dropdown, DropdownAdmin)
admin.site.register(Household)
