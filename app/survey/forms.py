from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Submit, Layout, ButtonHolder, Fieldset, Div, Button,
        MultiField, HTML)
from crispy_forms.bootstrap import InlineField, InlineRadios, Field, PrependedAppendedText, AppendedText, Accordion, AccordionGroup
from django import forms
from django.apps import apps
from django.db.models import Q
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
import numpy as np
import decimal

from .models import *


class BaseModelForm(forms.ModelForm):
    LABEL = {}
    PLACEHOLDERS = {}

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)


        model =  self.Meta.model.__name__
        dropdown_fields = [ field for field in self.fields if (self.fields[field].__class__.__name__ == 'CharField' or self.fields[field].__class__.__name__ == 'IntegerField' or self.fields[field].__class__.__name__ == 'DecimalField') ]

        for dropdown in Dropdown.objects.prefetch_related('dropdown_options').filter(model_name=model, field_name__in=dropdown_fields):
            choices = dropdown.get_options()

            if len(choices) > 1:
                self.fields[dropdown.field_name] = forms.ChoiceField(widget=forms.Select(), choices=choices, required=False)

        for key in self.LABEL:
            self.fields[key].label = self.LABEL[key]

        for key in self.PLACEHOLDERS:
            self.fields[key].widget.attrs['placeholder'] = self.PLACEHOLDERS[key]

    def clean(self):
        cleaned_data = super(BaseModelForm, self).clean()
        source_model = apps.get_model(app_label='survey', model_name=self.Meta.model.__name__)

        for field_name in cleaned_data:
            try:
                field_type = source_model._meta.get_field(field_name).__class__.__name__
            except Exception, ex:
                continue
            if ((field_type == 'IntegerField' or
                    field_type ==  'DecimalField') and
                    cleaned_data[field_name] == ''):
                cleaned_data[field_name] = None
        return cleaned_data


class ExistingParticipantForm(forms.Form):
    participant_number = forms.CharField(label='Fill in the participant number here', max_length=Participant.PARTICIPANT_NUMBER_LENGTH, required=False)

    def __init__(self, *args, **kwargs):
        super(ExistingParticipantForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.form_id = 'id-ExistingParticipantForm'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Submit('submit', 'Continue on an Existing Participant', css_class='btn-lg btn-primary btn-block'),
            InlineField('participant_number', css_class='form-control-lg'),
        )


    def clean_participant_number(self):
        participant_number = self.cleaned_data['participant_number']
        if participant_number:
            if not Participant.objects.filter(participant_number=participant_number).exists():
                raise forms.ValidationError('Participant number cannot be found.')
        else:
            raise forms.ValidationError('Please provide a participant number.')
        return participant_number


class GenInfoForm(BaseModelForm):
    household_code = forms.IntegerField(label='Household code', required=False)

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'former_occupation': 'If retired, former occupation',
            'age': 'Age (Years)',
        }

        super(GenInfoForm, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-4 col-md-3'
        self.helper.form_id = 'id-GenInfoForm'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Field('participant_number', type='hidden'),
            Div(
                Div(
                    HTML(' <h3 class="panel-title">Participant Details</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    Field('date_started', readonly=''),
                    'household_code',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Individual Details</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    Div(
                        HTML('<label class="col-sm-2">Name</label>'),
                        InlineField('given_name'),
                        InlineField('middle_name'),
                        InlineField('last_name'),
                        id='name-group',
                    ),
                    # 'region',
                    # 'province',
                    # 'barangay',
                    'birthday',
                    Field('age', readonly=''),
                    Div('occupation', 'former_occupation', css_class="form-group row"),
                    'sex',
                    'civil_status',
                    'contact_number',
                    'educational_attainment',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn-primary col-sm-2 custom-button'),
            )
        )

    def save(self, commit=True):
        instance = super(GenInfoForm, self).save(commit=False)

        if commit:
            instance.save()
        return instance

    class Meta:
        model = Participant
        fields = ['participant_number',
                'date_started',
                'given_name',
                'last_name',
                'middle_name',
                'birthday',
                'occupation',
                'age',
                'sex',
                'civil_status',
                'contact_number',
                'educational_attainment',
                'former_occupation',
            ]


class GeneralInfoForm(BaseModelForm):

    def __init__(self, *args, **kwargs):

        self.LABEL = {
            'age': 'Age (Years)',
            'former_occupation': 'If retired, former occupation',
        }

        self.PLACEHOLDERS = {
            'given_name': 'Given',
            'middle_name': 'Middle',
            'last_name': 'Last'
        }

        super(GeneralInfoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Participant
        fields = [
            'participant_number',
            'date_started',
            'given_name',
            'last_name',
            'middle_name',
            'birthday',
            'occupation',
            'age',
            'sex',
            'civil_status',
            'contact_number',
            'educational_attainment',
            'former_occupation',
        ]


class GenInfo_HouseholdForm(BaseModelForm):
    # tells whether form has participant instance
    has_instance = False

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'code': 'Household Code',
        }

        self.PLACEHOLDERS = {
            'nearest_relative_contact_details': "Relative's contact details",
        }

        if 'instance' in kwargs and kwargs['instance'].household_code:
            has_instance = True
            kwargs['instance'] = kwargs['instance'].household_code
        super(GenInfo_HouseholdForm, self).__init__(*args, **kwargs)


    def save(self, commit=True):
        instance = None
        if self.has_instance:
            instance = super(GenInfo_HouseholdForm, self).save(commit=commit)
        else:
            participant = super(GenInfo_HouseholdForm, self).save(commit=False)
            instance, created = Household.objects.get_or_create(code=self.cleaned_data['code'])

            for attr, value in self.cleaned_data.iteritems():
                setattr(instance, attr, value)

            if commit:
                instance.save()
                participant.household_code = instance
                participant.save()    
        return instance


    class Meta:
        model = Household
        fields = [
            'code',
            'region',
            'province',
            'barangay',
            'nearest_relative',
            'nearest_relative_contact_details',
        ]

class HealthLifestyleForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'bp_systolic': ' ',
            'bp_diastolic': ' ',
            'height': 'Height (cm)',
            'weight': 'Weight (kg)',
            'bmi': 'BMI (kg/m<sup>2</sup>)',
            'body_temperature': 'Temperature (&#8451;)',
            'glucose_level': 'Glucose level (mg/dL)',
            'number_of_hours_last_meal': 'Number of hours since last meal',
            'cholesterol_level': 'Cholesterol level (mg/dL)',
            'pulse_rate': 'Pulse rate (per minute)',
            'respiratory_rate': 'Respiratory rate (per minute)',
        }
        super(HealthLifestyleForm, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-5'
        self.helper.layout = Layout(
            Field('participant_number', type='hidden'),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Body Mass Index  <sup>2</sup></h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'height',
                    'weight',
                    Field('bmi', readonly=True),
                    Field('classification', readonly=True),
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Vital Signs</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'body_temperature',
                    Div(
                        HTML('<label class="col-sm-2">Blood pressure (mmHg)</label>'),
                        InlineField('bp_systolic', placeholder='Systolic'),
                        HTML('<span>&nbsp;</span>'),
                        InlineField('bp_diastolic', placeholder='Diastolic'),
                        id='bp-group',
                        css_class="col-sm-12",
                    ),
                    'pulse_rate',
                    'respiratory_rate',
                    'glucose_level',
                    'number_of_hours_last_meal',
                    'cholesterol_level',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
        )

    class Meta:
        model = Participant
        fields = [
            'participant_number',
            'height',
            'weight',
            'bmi',
            'classification',
            'body_temperature',
            'bp_systolic',
            'bp_diastolic',
            'pulse_rate',
            'respiratory_rate',
            'glucose_level',
            'number_of_hours_last_meal',
            'cholesterol_level',
        ]


class HealthLifestyleForm2(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'rice_plate_proportion': 'Rice',
            'beef_plate_proportion': 'Beef',
            'pork_plate_proportion': 'Pork',
            'chicken_plate_proportion': 'Chicken',
            'egg_plate_proportion': 'Egg',
            'fish_plate_proportion': 'Fish',
            'vegetables_plate_proportion': 'Vegetables',
            'junkfoods_plate_proportion': 'Junk Foods',
            'slices_of_bread': 'No. of bread slices/pieces',
            'bread_intake_frequency': 'Frequency',
            'glasses_of_milk': 'No. of glasses of milk',
            'milk_intake_frequency': 'Frequency',
            'fruit_juice_type': 'Fruit juice type',
            'glasses_of_fruit_juice': 'No. of glasses of fruit juice',
            'fruit_juice_intake_frequency': 'Frequency',
        }

        super(HealthLifestyleForm2, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-5'

        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(' <h3 class="panel-title">Diet and Nutrition</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'number_of_complete_meal_per_day',
                    'average_number_of_plate_per_meal',
                    Div(
                        Div(HTML('<h3 class="subpanel-title">Average Plate Proportion</h3>'), css_class='col-sm-offset-1'),
                        'rice_plate_proportion',
                        'beef_plate_proportion',
                        'pork_plate_proportion',
                        'chicken_plate_proportion',
                        'egg_plate_proportion',
                        'fish_plate_proportion',
                        'vegetables_plate_proportion',
                        'junkfoods_plate_proportion',
                        css_class='col-sm-offset-1',
                        id='average-plate-proportion-details'
                    ),
                    Div(
                        'slices_of_bread',
                        'bread_intake_frequency',
                        css_class='form-group row'
                    ),
                    Div(
                        'glasses_of_milk',
                        'milk_intake_frequency',
                        css_class='form-group row'
                    ),
                    Div(
                        'glasses_of_fruit_juice',
                        'fruit_juice_intake_frequency',
                        css_class='form-group row'
                    ),
                    'fruit_juice_type',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
        )
    class Meta:
        model = Participant
        fields = [
            'number_of_complete_meal_per_day',
            'average_number_of_plate_per_meal',
            'rice_plate_proportion',
            'beef_plate_proportion',
            'pork_plate_proportion',
            'chicken_plate_proportion',
            'egg_plate_proportion',
            'fish_plate_proportion',
            'vegetables_plate_proportion',
            'junkfoods_plate_proportion',
            'slices_of_bread',
            'glasses_of_milk',
            'fruit_juice_type',
            'glasses_of_fruit_juice',
            'bread_intake_frequency',
            'milk_intake_frequency',
            'fruit_juice_intake_frequency',
        ]


class HealthLifestyleForm3(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(HealthLifestyleForm3, self).__init__(*args, **kwargs)

        self.fields['hat_or_umbrella_frequency'].label = 'Frequency'

        self.fields['protective_glasses_type'].label = 'Type'
        self.fields['protective_glasses_frequency'].label = 'Frequency'

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-3'

        self.helper.layout = Layout(
            'use_protective_glasses',
            Div(
                'protective_glasses_type',
                'protective_glasses_frequency',
                css_class="col-sm-offset-1",
                id="glasses-details",
            ),
            'use_hat_or_umbrella',
            Div(
                'hat_or_umbrella_frequency',
                css_class='col-sm-offset-1',
                id="hat-umbrella-details"
            )
        )

    class Meta:
        model = Participant
        fields = [
            'use_protective_glasses',
            'protective_glasses_type',
            'protective_glasses_frequency',
            'use_hat_or_umbrella',
            'hat_or_umbrella_frequency',
        ]


class HealthLifestyleForm4(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'has_menstrual_period_ended': 'Has menstrual period ended?',
        }

        super(HealthLifestyleForm4, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-3'

        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(' <h3 class="panel-title">Women\'s Health</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'age_menstrual_period_started',
                    'has_menstrual_period_ended',
                    Div(
                        'age_menstrual_period_ended',
                        'type_of_menopausal',
                        css_class='col-sm-offset-1',
                        id="menopausal-details"
                    ),
                    'number_of_deliveries',
                    'usage_of_contraception',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
        )

    class Meta:
        model = Participant
        fields = [
            'age_menstrual_period_started',
            'has_menstrual_period_ended',
            'age_menstrual_period_ended',
            'type_of_menopausal',
            'number_of_deliveries',
            'usage_of_contraception',
        ]


class HealthLifestyleForm5(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'blindness_be_prevented': 'Do you think blindness can be prevented?<br />(<i>Sa tingin ninyo, paano maiiwasan ang pagkabulag?</i>)',
            'taking_care_of_blind_person': 'How would you take care of a blind person?<br />(<i>Paano matutulungan o maalagaan ang isang bulag?</i>)',
            'action_in_case_of_eye_disease': 'What would you do if you have eye problem?<br />(<i>Ano ang inyong gagawin kapag kayo ay may problema sa mata/paningin?</i>)',
            'action_in_case_of_health_problem': 'If you have any health problem (e.g. cough, colds), what would you do?<br />(<i>Kung kayo ay may karamdaman ( tulad ng ubo, sipon, ...), ano ang inyong gagawin?</i>)',
            'eye_checkup_place': 'Where is the nearest place to get your eyes checked? <br />(<i>Saan ang pinakamalapit na pwede ipacheck up ang inyong mata/paningin?</i>)',
            'philhealth_beneficiary': 'Do you have Philhealth? Do you avail its insurance? If not, why?<br />(<i>Kayo po ba ay may Philhealth? Nagagamit nyo po ba ito? Kung hindi, bakit po?</i>)',
            'eye_health_care_satisfaction': 'How satisfied are you with the government\'s assistance on eye health care?<br />(<i>Gaano kayo nasisiyahan/kontento sa mga serbisyong pangkalusugan na binigay ng gobyerno?</i>)',
        }

        super(HealthLifestyleForm5, self).__init__(*args, **kwargs)


        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_tag = False
        self.helper.label_class = 'col-sm-6'
        self.helper.field_class = 'col-sm-4 '

        self.helper.layout = Layout(
            Div(
                Div(
                    HTML(' <h3 class="panel-title">General Eye Health: Knowledge, Attitude, Practices and Access to Health Care</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'blindness_be_prevented',
                    HTML('<br />'),HTML('<br />'),
                    'taking_care_of_blind_person',
                    HTML('<br />'),HTML('<br />'),
                    'action_in_case_of_eye_disease',
                    HTML('<br />'),HTML('<br />'),
                    'eye_checkup_place',
                    HTML('<br />'),HTML('<br />'),
                    'action_in_case_of_health_problem',
                    HTML('<br />'),HTML('<br />'),
                    'philhealth_beneficiary',
                    HTML('<br />'),HTML('<br />'),
                    'eye_health_care_satisfaction',
                    css_class="panel-body eye-questions"
                ),
                css_class="panel panel-default"
            ),
        )

    class Meta:
        model = Participant
        fields = [
            'blindness_be_prevented',
            'taking_care_of_blind_person',
            'action_in_case_of_eye_disease',
            'eye_checkup_place',
            'eye_health_care_satisfaction',
            'action_in_case_of_health_problem',
            'philhealth_beneficiary',
        ]


class HealthLifestyleForm6(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'smoking_duration': 'Smoking Duration (years)',
            'recreational_drug_type': 'If yes, specify:',
            'recreational_drug_duration': 'Duration (months)',
            'alcohol_total_glasses': 'Total glasses',
        }

        super(HealthLifestyleForm6, self).__init__(*args, **kwargs)

        self.fields['alcohol_total_glasses'].widget.attrs['readonly'] = ''

    class Meta:
        model = Participant
        fields = [
            'insurance_holder',
            'dependent_of_insurance_holder',
            'smoker',
            'type_of_smoke',
            'number_of_sticks',
            'smoking_frequency',
            'smoking_duration',
            'pack_years',
            'alcoholic_beverages',
            'alcohol_total_glasses',
            'recreational_drug',
            'recreational_drug_type',
            'recreational_drug_duration'
        ]

class Location_HouseholdForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'gps_reading' : 'GPS Location Reading (Lat*Long)',
            'elevation': 'Elevation (meters)',
            'locale' : 'Area Classification',
            'distance_from_the_sea' : 'Distance from the Sea (from shoreline)',
            'length_of_stay' : 'Length of Stay (years)'
        }

        if 'instance' in kwargs and kwargs['instance'].household_code:
            kwargs['instance'] = kwargs['instance'].household_code
        super(Location_HouseholdForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Household
        fields = [
            'gps_reading',
            'elevation',
            'locale',
            'distance_from_the_sea',
            'length_of_stay',
        ]


class SunlightExposureForm(BaseModelForm):

    class Meta:
        model = Participant
        fields = [
            'number_of_years_elementary_sunlight_exposure',
            'number_of_years_highschool_sunlight_exposure',
            'number_of_years_college_sunlight_exposure',
            'number_of_years_working_sunlight_exposure',
            'number_of_years_retired_sunlight_exposure',
            'number_of_years_total_average_sunlight_exposure',
            'number_of_hours_spent_elementary_sunlight_exposure',
            'number_of_hours_spent_highschool_sunlight_exposure',
            'number_of_hours_spent_college_sunlight_exposure',
            'number_of_hours_spent_working_sunlight_exposure',
            'number_of_hours_spent_retired_sunlight_exposure',
            'number_of_hours_spent_total_average_sunlight_exposure',
        ]

class NearWorkForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'protective_glasses_type_near_work': 'Protective Glass Type',
            'frequency_using_glasses_near_work': 'Frequency of using glasses near work',
        }

        super(NearWorkForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Participant
        fields = [
            'number_of_years_elementary_near_work',
            'number_of_years_highschool_near_work',
            'number_of_years_college_near_work',
            'number_of_years_working_near_work',
            'number_of_years_retired_near_work',
            'number_of_years_total_average_near_work',
            'number_of_hours_spent_elementary_near_work',
            'number_of_hours_spent_highschool_near_work',
            'number_of_hours_spent_college_near_work',
            'number_of_hours_spent_working_near_work',
            'number_of_hours_spent_retired_near_work',
            'number_of_hours_spent_total_average_near_work',
            'number_of_hours_spent_near_computer_elementary_near_work',
            'number_of_hours_spent_near_computer_highschool_near_work',
            'number_of_hours_spent_near_computer_college_near_work',
            'number_of_hours_spent_near_computer_working_near_work',
            'number_of_hours_spent_near_computer_retired_near_work',
            'number_of_hours_spent_near_computer_total_average_near_work',
            'frequency_using_glasses_near_work',
            'protective_glasses_type_near_work',
        ]


class SocioEconomicForm(BaseModelForm):
    participant_number = forms.CharField(max_length=Participant.PARTICIPANT_NUMBER_LENGTH, required=False)

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'total_household_members': 'Total household members employed (past 6 months)',
            'domestic_service': 'Domestic Service (past 4 weeks)',
            'laundry_service': 'Laundry Service (past 4 weeks)',
            'household_member_traveled_by_plane': 'Household member traveled by plane paid by self or household (past 4 weeks)',
            'hired_school_transport': 'Hired school transport (past 4 weeks)',
            'financial_assistance_from_abroad': 'Financial assistance from abroad (past 12 months)',
            'stereo_cd_owned': 'Stereo/CD',
            'tv_owned': 'TV',
            'vhs_vcd_dvd_vtr_owned': 'VHS/VCD/DVD/VTR',
            'ref_freezer_owned': 'Refrigerator/Freezer',
            'washing_machine_owned': 'Washing Machine',
            'aircon_owned': 'Aircon',
            'computer_owned': 'Computer/Laptop',
            'phones_owned': 'Phones (Landline/Mobile)',
            'sala_sofa_owned': 'Sala Set/Sofa',
        }

        participant_number = None
        if 'instance' in kwargs and kwargs['instance'].household_code:
            participant_number = kwargs['instance'].participant_number
            kwargs['instance'] = kwargs['instance'].household_code

        super(SocioEconomicForm, self).__init__(*args, **kwargs)

        self.fields['participant_number'].initial = participant_number

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-3'
        self.helper.form_id = 'id-SocioEconomicForm'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Field('participant_number', type='hidden'),
            Div(
                Div(
                    'area',
                    'locale',
                    'total_household_members',
                    'household_type',
                    'household_head_civil_status',
                    'household_head_educational_attainment',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    HTML(' <h3 class="panel-title">Vehicle Ownership</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'number_of_cars',
                    'number_of_jeep_motorcycles_tricycles',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    'fuel_types_for_cooking',
                    'domestic_service',
                    'laundry_service',
                    'household_member_traveled_by_plane',
                    'household_member_enrolled_in_private_school',
                    'hired_school_transport',
                    'financial_assistance_from_abroad',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Durables/Facilities</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    Div(HTML('<h3 class="subpanel-title">Number of Durables/Facilities Owned</h3>'), css_class='col-sm-offset-2'),
                    'stereo_cd_owned',
                    'tv_owned',
                    'vhs_vcd_dvd_vtr_owned',
                    'ref_freezer_owned',
                    'washing_machine_owned',
                    'aircon_owned',
                    'computer_owned',
                    'phones_owned',
                    'sala_sofa_owned',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    'toilet_facility',
                    'main_water_source',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    'housewife_employment_status',
                    'household_head_work_sector',
                    'household_head_occupational_status',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    'house_ownership',
                    'number_of_senior_citizens',
                    'house_type',
                    'roof_type',
                    'outer_wall_type',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            Div(
                Div(
                    Field('socio_economic_cluster', readonly=True),
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn-primary col-sm-2 custom-button'),
            )
        )

    class Meta:
        model = Household
        fields = [
            'participant_number',
            'area',
            'locale',
            'total_household_members',
            'household_type',
            'household_head_civil_status',
            'household_head_educational_attainment',
            'number_of_cars',
            'number_of_jeep_motorcycles_tricycles',
            'fuel_types_for_cooking',
            'domestic_service',
            'laundry_service',
            'household_member_traveled_by_plane',
            'household_member_enrolled_in_private_school',
            'hired_school_transport',
            'financial_assistance_from_abroad',
            'stereo_cd_owned',
            'tv_owned',
            'vhs_vcd_dvd_vtr_owned',
            'ref_freezer_owned',
            'washing_machine_owned',
            'aircon_owned',
            'computer_owned',
            'phones_owned',
            'sala_sofa_owned',
            'toilet_facility',
            'main_water_source',
            'housewife_employment_status',
            'household_head_work_sector',
            'household_head_occupational_status',
            'house_ownership',
            'number_of_senior_citizens',
            'house_type',
            'roof_type',
            'outer_wall_type',
            'socio_economic_cluster',
        ]


class EyeExamForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(EyeExamForm, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-5'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Field('participant_number', readonly=True),
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn-primary col-sm-2 custom-button'),
            )
        )

    class Meta:
        model = Participant
        fields = [
            'participant_number' 
        ]


class DiagnosticsForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(DiagnosticsForm, self).__init__(*args, **kwargs)
        self.fields['slit_lamp_od'].label = 'OD'
        self.fields['slit_lamp_os'].label = 'OS'

        self.fields['fundus_od'].label = 'OD'
        self.fields['fundus_os'].label = 'OS'

        self.fields['optic_nerve_od'].label = 'OD'
        self.fields['optic_nerve_os'].label = 'OS'

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-5'
        self.helper.form_id = 'id-GenInfoForm'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Field('participant_number', type='hidden'),
            Div(
                Div(
                    HTML(' <h3 class="panel-title">Slit Lamp Photos</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'slit_lamp_od',
                    #Div(
                    #    HTML("""{% if form.slit_lamp_od.value %}<img class="img-responsive col-sm-10 col-sm-offset-2" style="height: 300px; width: 300px" src="/media/{{ form.slit_lamp_od.value }}">{% endif %}""", )),
                    'slit_lamp_os',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Fundus</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'fundus_od',
                    'fundus_os',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            Div(
                Div(
                    HTML(' <h3 class="panel-title">Optic</h3>'),
                    css_class="panel-heading"
                ),
                Div(
                    'optic_nerve_od',
                    'optic_nerve_os',
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),

            ButtonHolder(
                Submit('submit', 'Save', css_class='btn-primary col-sm-2 custom-button'),
            )
        )

    class Meta:
        model = Participant
        fields = [
            'participant_number',
            'slit_lamp_od',
            'slit_lamp_os',
            'fundus_od',
            'fundus_os',
            'optic_nerve_od',
            'optic_nerve_os',
        ]

class NotesForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        super(NotesForm, self).__init__(*args, **kwargs)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-5'
        self.helper.form_show_labels = False
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            Field('participant_number', type='hidden'),
            Div(
                Div(
                    Field('notes', placeholder="Type in your notes here."),
                    css_class="panel-body"
                ),
                css_class="panel panel-default"
            ),
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn-primary col-sm-2 custom-button'),
            )
        )

    class Meta:
        model = Participant
        fields = [
            'participant_number',
            'notes',
        ]


class SystemicDiseaseForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(SystemicDiseaseForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Systemic Disease'
        self.fields['month_year_diagnosed'].label = 'Month/Year Diagnosed'
        self.fields['duration_auto_calc'].label = 'Duration (months)'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.form_tag = False
        self.helper.form_id = 'id-SystemicDiseaseForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            Field('month_year_diagnosed', placeholder='mm/yyyy', css_class='monthpicker'),
            Field('duration_auto_calc', placeholder='months', readonly=True),
            'duration',
        )

    class Meta:
        model = SystemicDisease
        fields = [
            'name',
            'month_year_diagnosed',
            'duration_auto_calc',
            'duration',
        ]


class LifeStyleConditionForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(LifeStyleConditionForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Lifestyle Condition'
        self.fields['month_year_diagnosed'].label = 'Month/Year Diagnosed'
        self.fields['duration_auto_calc'].label = 'Duration (months)'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-8'
        self.helper.form_tag = False
        self.helper.form_id = 'id-LifeStyleConditionForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            Field('month_year_diagnosed', placeholder='mm/yyyy', css_class='monthpicker'),
            Field('duration_auto_calc', placeholder='months', readonly=True),
            'duration',
        )

    class Meta:
        model = LifeStyleCondition
        fields = [
            'name',
            'month_year_diagnosed',
            'duration_auto_calc',
            'duration',
        ]

class SystemicMedicationForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(SystemicMedicationForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Systemic Medication'
        self.fields['month_year_started'].label = 'Month/Year Started'
        self.fields['duration_auto_calc'].label = 'Duration (months)'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-8'
        self.helper.form_tag = False
        self.helper.form_id = 'id-SystemicMedicationForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            'dose',
            Field('month_year_started', placeholder='mm/yyyy', css_class='monthpicker'),
            Field('duration_auto_calc', placeholder='months', readonly=True),
            'duration',
        )

    class Meta:
        model = SystemicMedication
        fields = [
            'name',
            'dose',
            'month_year_started',
            'duration_auto_calc',
            'duration',
        ]


class DietarySupplementForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(DietarySupplementForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Dietary Supplement'
        self.fields['month_year_started'].label = 'Month/Year Started'
        self.fields['duration_auto_calc'].label = 'Duration (months)'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-8'
        self.helper.form_tag = False
        self.helper.form_id = 'id-DietarySupplementForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            'dose',
            Field('month_year_started', placeholder='mm/yyyy', css_class='monthpicker'),
            Field('duration_auto_calc', placeholder='months', readonly=True),
            'duration',
        )

    class Meta:
        model = DietarySupplement
        fields = [
            'name',
            'dose',
            'month_year_started',
            'duration_auto_calc',
            'duration',
        ]


class OcularMedicationForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(OcularMedicationForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Ocular Medication'
        self.fields['month_year_started'].label = 'Month/Year Started'
        self.fields['duration_auto_calc'].label = 'Duration (months)'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-8'
        self.helper.label_class = 'col-sm-3'
        self.helper.form_tag = False
        self.helper.form_id = 'id-OcularMedicationForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            'dose',
            'eye',
            Field('month_year_started', placeholder='mm/yyyy', css_class='monthpicker'),
            Field('duration_auto_calc', placeholder='months', readonly=True),
            'duration',
        )

    class Meta:
        model = OcularMedication
        fields = [
            'name',
            'dose',
            'eye',
            'month_year_started',
            'duration_auto_calc',
            'duration',
        ]

class OcularSymptomForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(OcularSymptomForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Ocular Symptom'
        self.fields['duration'].label = 'Duration (months)'
        self.fields['main_symptom_od'].label = 'Main Symptom OD'
        self.fields['main_symptom_os'].label = 'Main Symptom OS'
        self.fields['main_symptom_patient'].label = 'Main Symptom Patient'

        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-8'
        self.helper.form_tag = False
        self.helper.form_id = 'id-OcularSymptomForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            'eye',
            Field('duration', placeholder='months'),
            Field('main_symptom_od', css_class='checkbox-group-1'),
            Field('main_symptom_os', css_class='checkbox-group-2'),
            Field('main_symptom_patient', css_class='checkbox-group-3'),
        )

    class Meta:
        model = OcularSymptom
        fields = [
            'name',
            'eye',
            'duration',
            'main_symptom_od',
            'main_symptom_os',
            'main_symptom_patient',
        ]


class NonOcularSymptomForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(NonOcularSymptomForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Non-Ocular Symptom'
        self.fields['duration'].label = 'Duration (months)'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-4'
        self.helper.form_tag = False
        self.helper.form_id = 'id-NonOcularSymptomForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.layout = Layout(
            'name',
            Field('duration', placeholder='months'),
        )

    class Meta:
        model = NonOcularSymptom
        fields = [
            'name',
            'duration',
        ]


class OcularFindingDiagnosisForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(OcularFindingDiagnosisForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Finding and diagnosis'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-4'
        self.helper.form_id = 'id-OcularFindingDiagnosisForm'
        self.helper.form_tag = False
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.layout = Layout(
            'name',
            'eye',
            Field('main_diagnosis_od', css_class='checkbox-group-4'),
            Field('main_diagnosis_os', css_class='checkbox-group-5'),
            Field('main_diagnosis_patient', css_class='checkbox-group-6'),
            Field('main_cause_visual_loss', css_class='checkbox-group-7'),
        )

    class Meta:
        model = OcularFindingDiagnosis
        fields = [
            'name',
            'eye',
            'main_diagnosis_od',
            'main_diagnosis_os',
            'main_diagnosis_patient',
            'main_cause_visual_loss',
        ]


class OcularProcedureForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(OcularProcedureForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Ocular Procedure'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-4'
        self.helper.form_id = 'id-OcularProcedureForm'
        self.helper.form_tag = False
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.layout = Layout(
            'name',
            'eye',
            'surgery_outcome',
        )

    class Meta:
        model = OcularProcedure
        fields = [
            'name',
            'eye',
            'surgery_outcome',
        ]


class ManagementForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super(ManagementForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Management'
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-4'
        self.helper.form_id = 'id-ManagementForm'
        self.helper.form_tag = False
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.layout = Layout(
            'name',
            'eye',
        )

    class Meta:
        model = OcularProcedure
        fields = [
            'name',
            'eye',
        ]


class AlcoholicBeverageForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'drinking_duration': 'Drinking Duration (Months)',
            'drinking_total_glasses': 'Alcohol Consumption (Subtotal Glasses)'
        }
        super(AlcoholicBeverageForm, self).__init__(*args, **kwargs)
        
        # Form Helper
        self.helper = FormHelper()
        self.helper.form_class = 'form-inline col-sm-4'
        self.helper.form_id = 'id-AlcoholicBeverageForm'
        self.helper.form_tag = False
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.layout = Layout(
            'alcoholic_type',
            'number_of_glasses',
            'drinking_frequency',
            'drinking_duration',
            'drinking_total_glasses'
        )

    class Meta:
        model = AlcoholicBeverage
        fields = [
            'alcoholic_type',
            'number_of_glasses',
            'drinking_frequency',
            'drinking_duration',
            'drinking_total_glasses',
        ]


class VisualAcuityForm(BaseModelForm):

    class Meta:
        model = Participant
        fields = [
            'visual_acuity_od_without_correction',
            'visual_acuity_od_with_correction',
            'visual_acuity_od_without_correction_and_pinhole',
            'visual_acuity_od_best_vision',
            'visual_acuity_near_vision',
            'visual_acuity_os_without_correction',
            'visual_acuity_os_with_correction',
            'visual_acuity_os_without_correction_and_pinhole',
            'visual_acuity_os_best_vision',
            'participant_number',
        ]


class AutomatedRefractionForm(BaseModelForm):

    class Meta:
        model = Participant
        fields = [
            'automated_refraction_od_sphere',
            'automated_refraction_od_cylinder',
            'automated_refraction_od_axis',
            'automated_refraction_os_sphere',
            'automated_refraction_os_cylinder',
            'automated_refraction_os_axis',
            'automated_refraction_pupil_distance',
        ]


class EyeGlassesForm(BaseModelForm):

    class Meta:
        model = Participant
        fields = [
            'type_eye_glasses_od',
            'type_eye_glasses_os',
            'eye_glass_refraction_od_sphere',
            'eye_glass_refraction_od_cylinder',
            'eye_glass_refraction_od_axis',
            'eye_glass_refraction_od_add',
            'eye_glass_refraction_os_sphere',
            'eye_glass_refraction_os_cylinder',
            'eye_glass_refraction_os_axis',
            'eye_glass_refraction_os_add',
        ]


class TonometryDilationGlusoseLevelBPForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {
            'bp_systolic': 'Systolic',
            'bp_diastolic': 'Diastolic',
            'glucose_level': 'Glucose level (mg/dL)',
            'for_tonometry': 'For Tonometry',
            'for_dilation': 'For Dilation',
        }

        super(TonometryDilationGlusoseLevelBPForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Participant
        fields = [
            'intraocular_pressure_od',
            'intraocular_pressure_os',
            'dilation_od',
            'dilation_os',
            'glucose_level',
            'number_of_hours_last_meal',
            'bp_systolic',
            'bp_diastolic',
            'for_tonometry',
            'for_tonometry_eye',
            'for_tonometry_consented', 
            'for_dilation',
            'for_dilation_eye',
            'for_dilation_consented',           
        ]


class IshiharaForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        self.LABEL = {}

        self.PLACEHOLDERS = {
            'ishihara_result': 'Result / Interpretation',
            'ishihara_number_of_plates_correct': '# of plates answered correctly',
            'ishihara_plates': '# of Ishihara plates',
        }

        super(IshiharaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Participant
        fields = [
            'ishihara_result',
            'ishihara_number_of_plates_correct',
            'ishihara_plates',        
        ]

class DropdownForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(DropdownForm, self).__init__(*args, **kwargs)

        app = apps.get_app_config('survey')
        field_options = []
        model_options = []

        for model_instance in app.get_models():
            model_options.append(model_instance.__name__)
            field_options += [ (field.name, field.name) for field in  model_instance._meta.fields if (field.__class__.__name__ == 'CharField' or field.__class__.__name__ == 'DecimalField' or field.__class__.__name__ == 'IntegerField') ]

        field_options = list(set(field_options))
        field_options.sort()

        self.fields['model_name'] = forms.ChoiceField(widget=forms.Select(), choices=[(model, model) for model in model_options])
        self.fields['field_name'] = forms.ChoiceField(widget=forms.Select(), choices=field_options)

    class Meta:
        model = Dropdown
        fields = ['model_name', 'field_name', 'display_name']


class DropdownTabForm(forms.ModelForm):
    class Meta:
        model = Dropdown
        fields = ['number_range', 'number_range_increment']


class DropdownListingForm(forms.Form):
    variables = forms.ModelChoiceField(queryset=Dropdown.objects.all().order_by('display_name'), required=False)
    add_multiple_options = forms.CharField(max_length=10000, 
        widget=forms.Textarea( attrs={'placeholder':'Add options separated by new line.'} ), 
        required=False)


class DropdownOptionForm(forms.ModelForm):
    replacement = forms.CharField(max_length=200, required=False)

    def __init__(self, *args, **kwargs):
        super(DropdownOptionForm, self).__init__(*args, **kwargs)

        if 'instance' in kwargs:
            options = kwargs['instance'].dropdown.dropdown_options.all().order_by('value')
            replacement_choice = [ (option.value, option.value) for option in options if option.id != self.instance.id ]
            
            replacement_choice.insert(0, ('', ''))
            self.fields['replacement'] = forms.ChoiceField(widget=forms.Select(), choices=replacement_choice, required=False)

        # Form Helper
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_id= 'dropdownOptionForm'
        self.helper.template = 'bootstrap/table_inline_formset.html'
        self.helper.layout = Layout(
            Field('value', readonly=True),
            'order',
            Field('replacement', css_class='col-sm-1'),
        )

    class Meta:
        model = DropdownOption
        fields = ['value', 'order']


class ConditionForm(forms.Form):
    operations = [
            ('', ''),
            ('=', '='),
            ('>', '>'),
            ('>=', '>='),
            ('<', '<'),
            ('<=', '<='),
            ('!=', '!='),
            ('CONTAINS', 'CONTAINS'),
        ]
    conjuctions = [
            ('', ''),
            ('AND', 'AND'),
            ('OR', 'OR'),
        ]
    variable = forms.ModelChoiceField(queryset=Dropdown.objects.all().order_by('display_name'))
    operation = forms.ChoiceField(choices=operations)
    value = forms.CharField(max_length=200)
    conjunction = forms.ChoiceField(choices=conjuctions, required=False)