var AREA  = {
	'Bicol': 1,
	'Metro Manila': 7, 
	'Mindanao': 0,
	'North/Central Luzon': 2,
	'South Luzon': 3,
	'Visayas': 1,
}

var LOCAL = {
	'Rural': 0,
	'Urban': 2
}

var HHOLD_TYPE = {
	'Extended w/ non-related families or persons': 1,
	'Single/Nuclear': 3
}

var HHOLD_HEAD_CIVIL_STATUS = {
	'Married': 1,
	'Not Married (single/separated/widowed/divorced)': 3
}

var LAUNDRY_SERV = {
	'Not paying for laundry': 0,
	'Paying for laundry': 4
}

var TRAVELED = {
	'None': 0,
	'Has': 5
}

var PRIVATE_SCHOOL = {
	'None': 0,
	'Has': 4
}

var HIRED_SCHOOL = {
	'Not paying for hired school transport': 0,
	'Paying for hired school transport': 3
}

var FINANCIAL = {
	'None': 0,
	'Has': 2
}

var WATER_SOURCE = {
	'Others (dug well, stream, rain, peddler, etc)': 1,
	'Shared': 2,
	'Own use': 3
}

var HW_EMPLOYMENT_STAT = {
	'Not employed': 0,
	'Employed (full/part-time)/self-employed': 2
}

var HHEAD_WORK_SECTOR = {
	'No job': 0,
	'Agri/fishery': 1,
	'Trade (stores), eatery': 2,
	'Others': 3
}

var HHEAD_OCC_STATUS = {
	'No job': 0,
	'Others': 1,
	"Managers, supervisors, gov't officials, corporate executives": 2
}

var HHEAD_EDUC_ATT = {
	'No schooling': 0,
	'Elementary': 2,
	'High school': 3,
	'Vocational (Tech course)/ College undergraduate': 4,
	'College graduate': 6,
	'Graduate (MBA degree holder)': 10
}

var FUEL_TYPES = {
	'Not use any below': 0,
	'Wood': 1,
	'Charcoal': 1, 
	'LPG': 4
}

var DOMESTIC_SERV = {
	'No helper/boy/driver': 0,
	'Has paid helper/boy/driver': 7
}

var TOILET_FACILITY = {
	'None': 0,
	'Others': 3,
	'Open-pit': 1,
	'Closed-pit': 2,
	'Water Sealed (with flush)': 4
}

var HOUSE_OWNERSHIP = {
	'Not owned': 1,
	'Owned': 2
}

var HOUSE_TYPE = {
	'Single detached': 0,
	'Duplex': 0,
	'Apartment/townhouse/condo': 2,
	'Others': 0
}

var ROOF_TYPE = {
	'Salvaged/makeshift materials': 0,
	'Mixed but predominantly salvaged materias': 1,
	'Light materials (cogon, nipa, anahaw)': 2,
	'Mixed but predominantly light materials': 2,
	'Mixed but predominantly strong materials': 3,
	'Strong Materials (galvanized, iron, all)': 4,
}

var WALL_TYPE = {
	'Salvaged/makeshift materials': 0,
	'Mixed but predominantly salvaged materias': 1,
	'Light materials (cogon, nipa, anahaw)': 2,
	'Mixed but predominantly light materials': 2,
	'Mixed but predominantly strong materials': 3,
	'Strong Materials (galvanized, iron, all)': 4
}

var MAPPING = {
	'id_area': AREA,
	'id_locale': LOCAL,
	'id_household_type': HHOLD_TYPE,
	'id_household_head_civil_status': HHOLD_HEAD_CIVIL_STATUS,
	'id_laundry_service': LAUNDRY_SERV,
	'id_household_member_traveled_by_plane': TRAVELED,
	'id_household_member_enrolled_in_private_school': PRIVATE_SCHOOL,
	'id_hired_school_transport': HIRED_SCHOOL,
	'id_financial_assistance_from_abroad': FINANCIAL,
	'id_main_water_source': WATER_SOURCE,
	'id_housewife_employment_status': HW_EMPLOYMENT_STAT,
	'id_household_head_work_sector': HHEAD_WORK_SECTOR,
	'id_household_head_occupational_status': HHEAD_OCC_STATUS,
	'id_household_head_educational_attainment': HHEAD_EDUC_ATT,
	'id_fuel_types_for_cooking': FUEL_TYPES,
	'id_domestic_service': DOMESTIC_SERV,
	'id_toilet_facility': TOILET_FACILITY,
	'id_house_ownership': HOUSE_OWNERSHIP,
	'id_house_type': HOUSE_TYPE,
	'id_roof_type':	ROOF_TYPE,
	'id_outer_wall_type': WALL_TYPE 
}

var MULTIPLIERS = {
	'id_total_household_members': 4,
	'id_number_of_cars': 4,
	'id_number_of_jeep_motorcycles_tricycles': 2,
	'id_tv_owned': 2,
	'id_ref_freezer_owned': 4,
	'id_aircon_owned': 2,
	'id_computer_owned': 2,
	'id_phones_owned': 2,
	'id_sala_sofa_owned': 2,
	//'id_number_of_senior_citizens': -2,
	'id_stereo_cd_owned': 1,
	'id_vhs_vcd_dvd_vtr_owned': 1,
	'id_washing_machine_owned': 1
}

var CLUSTER = {
	'9': [67, 1000],
	'8': [55, 66],
	'7': [49, 54],
	'6': [44, 48],
	'5': [39, 43],
	'4': [35, 38],
	'3': [30, 34],
	'2': [24, 29],
	'1': [0, 23],
}

function calculateScore(){
	var result = 0;
	$('.scoring').remove();
	for(var key in MAPPING){
		var element = $('#'+key);
		if(element.val() != null && element.val() != ''){
			var points = MAPPING[key][element.val()];
			result += points;
			element.parent().after('<div class="col-sm-2 scoring">Points: '+ points +'</div>');
		}
		
	}

	for(var key in MULTIPLIERS){
		var element = $('#'+key);
		if(element.val() != null && element.val() != ''){
			var points = MULTIPLIERS[key]*parseInt(element.val());
			result += points;
			element.parent().after('<div class="col-sm-2 scoring">Points: '+ points +'</div>');
		}
	}

	var number_of_senior = $('#id_number_of_senior_citizens').val();
	if(number_of_senior != null && number_of_senior != ''){
		var points = (parseInt(number_of_senior) - 2);
		result += points;
		$('#id_number_of_senior_citizens').parent().after('<div class="col-sm-2 scoring">Points: '+ points +'</div>');
	}

	var cluster = '';
	for(var key in CLUSTER){
		if(CLUSTER[key][0] <= result && CLUSTER[key][1] >= result){
			cluster = key;
			break;
		}
	}
	$('#id_socio_economic_cluster').val(cluster);
	$('#id_socio_economic_cluster').parent().after('<div class="col-sm-2 scoring">Total Points: '+result+'</div>');
}

$(function(){
	// SCORING
	calculateScore();
	for(var key in MAPPING){
		var element = $('#'+key).change(function(){
			calculateScore();
		});
	}

	for(var key in MULTIPLIERS){
		var element = $('#'+key).change(function(){
			calculateScore();
		});
	}

	$('#id_number_of_senior_citizens').change(function(){  calculateScore(); })

	if(showScores){
		$('.scoring').show();
	}
});