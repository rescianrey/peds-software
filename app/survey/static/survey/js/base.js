$(function() {
    initialize($(this));

    var validator = function(){
        if($('#pnumber').val() == ""){
            alert('Please provide a participant number first.');
            $('#profile-pic').val('');
            return false;
        }
        return true;
    }

    $('#pnumber').change(function(){
        $('#id_participant_number').val($(this).val());
    });
    
    makeImagesUploadable(validator);

    $('#is_household_head').change(function(){
        $.post(
                HHeadUpdateURL,
                {   
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'pn': $('#pnumber').val(),
                    'is_household_head': $('#is_household_head').is(':checked')
                }
            );
    });

    if(displayPP){
        showProfilePic();

        $(window).resize(function(){
            $('.participant-info').hide();
            $('.participant-state').hide();
            showProfilePic();
        });
    }

});

function showProfilePic(){
    $('.participant-info').css('left', ($(window).width() - $('.participant-info').width() - 20));
        $('.participant-info').css('top', $('.top-bar').height());
        $('.participant-info').fadeIn();

        $('.participant-state').css('left', ($(window).width() - $('.participant-info').width() - 20));
        $('.participant-state').css('top', $('.top-bar').height() + $('.participant-info').height() + 35);


        setInterval(function(){
            var pn = $('#pnumber').val();
            var url = TONOMETRY_DILATION_STATUS + '?pn=' + pn +'&timestamp='+(new Date()).getTime();
            if(pn != null){
                $.get(url, function(details, status, xhr){
                    if(details.for_tonometry && (details.for_tonometry_consented == null || details.for_tonometry_consented == '')){
                        var text = 'For tonometry' + (details.for_tonometry_eye==null||details.for_tonometry_eye==''?'':', ' + details.for_tonometry_eye);
                        $('#for-tonometry-display').html(text);
                    }else{
                        $('#for-tonometry-display').html('');
                    }

                    if(details.for_dilation && (details.for_dilation_consented == null || details.for_dilation_consented == '')){
                        var text = 'For dilation' + (details.for_dilation_eye==null||details.for_dilation_eye==''?'':', ' + details.for_dilation_eye);
                        $('#for-dilation-display').html(text);
                        $('.participant-state').show();
                    }else{
                        $('#for-dilation-display').html('');
                    }

                    if($('#for-tonometry-display').html() == '' && $('#for-dilation-display').html() == ''){
                        $('.participant-state').hide();
                    }else{
                        $('.participant-state').show();
                    }
                });
            }
        }, 3000);
}

