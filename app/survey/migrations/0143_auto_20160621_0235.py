# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-06-21 02:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0142_auto_20160603_1139'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='amsler_od',
            field=models.ImageField(blank=True, null=True, upload_to=b''),
        ),
        migrations.AddField(
            model_name='participant',
            name='amsler_os',
            field=models.ImageField(blank=True, null=True, upload_to=b''),
        ),
    ]
