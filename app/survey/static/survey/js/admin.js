if (!$) {
    $ = django.jQuery;
}

$(function() {
    // Transform all date input types to date widgets
    $('select').selectize({sortField: 'text'});
})