function isChecked(jQelement){
    return jQelement.is(':checked')?'True':'False'
}

function stringValue(jQelement){
    return "'"+ jQelement.val() +"'"
}

$(function(){
    $('.special-col .selectize-input').css('height', '85px');
    $('.special-col input.form-control').css('height', '85px');
    $('.special-col').css('background-color', 'white');
    $('th[for="id_ocularsymptom_set-0-eye"]').addClass('col-sm-2');
    $('th[for="id_ocularsymptom_set-0-duration"]').addClass('col-sm-2');
    $('th[for="id_systemicmedication_set-0-dose"]').addClass('col-sm-2');
    $('th[for="id_ocularmedication_set-0-dose"]').addClass('col-sm-2');
    $('th[for="id_ocularmedication_set-0-eye"]').addClass('col-sm-2');
    $('th[for="id_ocularfindingdiagnosis_set-0-eye"]').addClass('col-sm-2');
    $('th[for="id_ocularprocedure_set-0-eye"]').addClass('col-sm-2');

    $('.special-col').each(function(){
        $('.item', this).each(function(){
            $(this).css('margin-top', '20px');
        });
    })

    showDivOnlyIf('tonometry-consent-div', 'id_for_tonometry', true);
    showDivOnlyIf('tonometry-details-div', 'id_for_tonometry', true);

    showDivOnlyIf('dilation-consent-div', 'id_for_dilation', true);
    showDivOnlyIf('dilation-details-div', 'id_for_dilation', true);

    var pn = $('#pnumber').val();
    var csrf = $('input[name="csrfmiddlewaretoken"]').val();

    saveOnChange(['for_tonometry', 'for_tonometry_eye', 'for_tonometry_consented'], [isChecked, stringValue, stringValue], pn, csrf);
    saveOnChange(['for_dilation', 'for_dilation_eye', 'for_dilation_consented'], [isChecked, stringValue, stringValue], pn, csrf);

    var amsler_clone = $("#amsler").clone();
    $("#amsler").remove();
    $("#ishihara").after(amsler_clone);

    $('#submit').click(function(){
        $("#form1").submit();
    });
});