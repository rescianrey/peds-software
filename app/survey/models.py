from __future__ import unicode_literals

import datetime
import numpy as np
import decimal

from django.apps import apps
from django.db import models

from sorl.thumbnail import ImageField

# Participant
class Participant(models.Model):
    '''
    Model for the Participant object
    '''
    PARTICIPANT_NUMBER_LENGTH = 5

    participant_number = models.IntegerField(unique=True)
    is_household_head = models.BooleanField(default=False)
    date_started = models.DateField('Date', null=True, blank=True)
    household_code = models.ForeignKey('Household', null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    given_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    middle_name = models.CharField(max_length=100, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    occupation = models.CharField(max_length=100, null=True, blank=True)
    former_occupation = models.CharField(max_length=100, null=True, blank=True)
    sex = models.CharField(max_length=100, null=True, blank=True)
    civil_status = models.CharField(max_length=100, null=True, blank=True)
    contact_number = models.CharField(max_length=20, null=True, blank=True)
    educational_attainment = models.CharField(max_length=100, null=True, blank=True)
    height = models.IntegerField(null=True, blank=True)
    weight = models.IntegerField(null=True, blank=True)
    bmi = models.DecimalField('BMI', max_digits=5, decimal_places=2, null=True, blank=True)
    classification = models.CharField(max_length=100, null=True, blank=True)
    body_temperature = models.CharField(max_length=100, null=True, blank=True)
    bp_systolic = models.CharField(max_length=100, null=True, blank=True)
    bp_diastolic = models.CharField(max_length=100, null=True, blank=True)
    pulse_rate = models.CharField(max_length=100, null=True, blank=True)
    respiratory_rate = models.CharField(max_length=100, null=True, blank=True)
    glucose_level = models.CharField(max_length=100, null=True, blank=True)
    cholesterol_level = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_last_meal = models.CharField(max_length=100, null=True, blank=True)
    insurance_holder = models.CharField(max_length=100, null=True, blank=True)
    dependent_of_insurance_holder = models.CharField(max_length=100, null=True, blank=True)
    smoker = models.CharField(max_length=100, null=True, blank=True)
    type_of_smoke = models.CharField(max_length=100, null=True, blank=True)
    number_of_sticks = models.CharField(max_length=100, null=True, blank=True)
    smoking_frequency = models.CharField(max_length=100, null=True, blank=True)
    smoking_duration = models.CharField(max_length=100, null=True, blank=True)
    pack_years = models.CharField(max_length=100, null=True, blank=True)
    alcoholic_beverages = models.CharField(max_length=100, null=True, blank=True)
    alcohol_total_glasses = models.IntegerField(null=True, blank=True)

    recreational_drug = models.CharField(max_length=100, null=True, blank=True)
    recreational_drug_type = models.CharField(max_length=100, null=True, blank=True)
    recreational_drug_duration = models.CharField(max_length=100, null=True, blank=True)

    number_of_complete_meal_per_day = models.CharField(max_length=100, null=True, blank=True)
    average_number_of_plate_per_meal = models.CharField(max_length=100, null=True, blank=True)
    rice_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    beef_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    pork_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    chicken_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    egg_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    fish_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    vegetables_plate_proportion = models.CharField(max_length=100, null=True, blank=True)
    junkfoods_plate_proportion = models.CharField(max_length=100, null=True, blank=True)

    slices_of_bread = models.CharField(max_length=100, null=True, blank=True)
    bread_intake_frequency = models.CharField(max_length=100, null=True, blank=True)

    glasses_of_milk = models.CharField(max_length=100, null=True, blank=True)
    milk_intake_frequency = models.CharField(max_length=100, null=True, blank=True)

    fruit_juice_type = models.CharField(max_length=100, null=True, blank=True)
    glasses_of_fruit_juice = models.CharField(max_length=100, null=True, blank=True)
    fruit_juice_intake_frequency = models.CharField(max_length=100, null=True, blank=True)

    number_of_years_elementary_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_highschool_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_college_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_working_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_retired_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_total_average_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)

    number_of_hours_spent_elementary_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_highschool_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_college_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_working_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_retired_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_total_average_sunlight_exposure = models.CharField(max_length=100, null=True, blank=True)
    
    use_protective_glasses = models.CharField(max_length=100, null=True, blank=True)
    protective_glasses_type = models.CharField(max_length=100, null=True, blank=True)
    protective_glasses_frequency = models.CharField(max_length=100, null=True, blank=True)

    use_hat_or_umbrella = models.CharField(max_length=100, null=True, blank=True)
    hat_or_umbrella_frequency = models.CharField(max_length=100, null=True, blank=True)

    number_of_years_elementary_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_highschool_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_college_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_working_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_retired_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_years_total_average_near_work = models.CharField(max_length=100, null=True, blank=True)

    number_of_hours_spent_elementary_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_highschool_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_college_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_working_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_retired_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_total_average_near_work  = models.CharField(max_length=100, null=True, blank=True)

    number_of_hours_spent_near_computer_elementary_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_near_computer_highschool_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_near_computer_college_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_near_computer_working_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_near_computer_retired_near_work = models.CharField(max_length=100, null=True, blank=True)
    number_of_hours_spent_near_computer_total_average_near_work  = models.CharField(max_length=100, null=True, blank=True)
    frequency_using_glasses_near_work = models.CharField(max_length=100, null=True, blank=True)
    protective_glasses_type_near_work = models.CharField(max_length=100, null=True, blank=True)

    age_menstrual_period_started = models.CharField(max_length=100, null=True, blank=True)
    has_menstrual_period_ended = models.CharField(max_length=100, null=True, blank=True)
    age_menstrual_period_ended = models.CharField(max_length=100, null=True, blank=True)
    type_of_menopausal = models.CharField(max_length=100, null=True, blank=True)
    number_of_deliveries = models.CharField(max_length=100, null=True, blank=True)
    usage_of_contraception = models.CharField(max_length=100, null=True, blank=True)

    blindness_be_prevented = models.CharField(max_length=100, null=True, blank=True)
    taking_care_of_blind_person = models.CharField(max_length=100, null=True, blank=True)
    action_in_case_of_eye_disease = models.CharField(max_length=100, null=True, blank=True)
    action_in_case_of_health_problem = models.CharField(max_length=100, null=True, blank=True)
    eye_checkup_place = models.CharField(max_length=100, null=True, blank=True)
    eye_health_care_satisfaction = models.CharField(max_length=100, null=True, blank=True)
    philhealth_beneficiary = models.CharField(max_length=100, null=True, blank=True)

    total_household_members = models.CharField(max_length=100, null=True, blank=True)
    household_type = models.CharField(max_length=100, null=True, blank=True)
    household_head_civil_status = models.CharField(max_length=100, null=True, blank=True)
    household_head_educational_attainment = models.CharField(max_length=100, null=True, blank=True)

    number_of_cars = models.CharField(max_length=100, null=True, blank=True)
    number_of_jeep_motorcycles_tricycles = models.CharField(max_length=100, null=True, blank=True)

    fuel_types_for_cooking = models.CharField(max_length=100, null=True, blank=True)
    domestic_service = models.CharField(max_length=100, null=True, blank=True)
    laundry_service = models.CharField(max_length=100, null=True, blank=True)
    household_member_traveled_by_plane = models.CharField(max_length=100, null=True, blank=True)
    household_member_enrolled_in_private_school = models.CharField(max_length=100, null=True, blank=True)
    hired_school_transport = models.CharField(max_length=100, null=True, blank=True)
    financial_assistance_from_abroad = models.CharField(max_length=100, null=True, blank=True)
    
    stereo_cd_owned = models.CharField(max_length=100, null=True, blank=True)
    tv_owned = models.CharField(max_length=100, null=True, blank=True)
    vhs_vcd_dvd_vtr_owned = models.CharField(max_length=100, null=True, blank=True)
    ref_freezer_owned = models.CharField(max_length=100, null=True, blank=True)
    washing_machine_owned = models.CharField(max_length=100, null=True, blank=True)
    aircon_owned = models.CharField(max_length=100, null=True, blank=True)
    computer_owned = models.CharField(max_length=100, null=True, blank=True)
    phones_owned = models.CharField(max_length=100, null=True, blank=True)
    sala_sofa_owned = models.CharField(max_length=100, null=True, blank=True)

    toilet_facility = models.CharField(max_length=100, null=True, blank=True)
    main_water_source = models.CharField(max_length=100, null=True, blank=True)

    housewife_employment_status = models.CharField(max_length=100, null=True, blank=True)
    household_head_work_sector = models.CharField(max_length=100, null=True, blank=True)
    household_head_occupational_status = models.CharField(max_length=100, null=True, blank=True)

    house_ownership = models.CharField(max_length=100, null=True, blank=True)
    number_of_senior_citizens = models.CharField(max_length=100, null=True, blank=True)
    house_type = models.CharField(max_length=100, null=True, blank=True)
    roof_type = models.CharField(max_length=100, null=True, blank=True)
    outer_wall_type = models.CharField(max_length=100, null=True, blank=True)
    socio_economic_cluster = models.CharField(max_length=100, null=True, blank=True)

    visual_acuity_od_without_correction = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_od_with_correction = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_od_without_correction_and_pinhole = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_near_vision = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_od_best_vision = models.CharField(max_length=100, null=True, blank=True)

    visual_acuity_os_without_correction = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_os_with_correction = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_os_without_correction_and_pinhole = models.CharField(max_length=100, null=True, blank=True)
    visual_acuity_os_best_vision = models.CharField(max_length=100, null=True, blank=True)

    automated_refraction_od_sphere = models.CharField(max_length=100, null=True, blank=True)
    automated_refraction_od_cylinder = models.CharField(max_length=100, null=True, blank=True)
    automated_refraction_od_axis = models.CharField(max_length=100, null=True, blank=True)

    automated_refraction_os_sphere = models.CharField(max_length=100, null=True, blank=True)
    automated_refraction_os_cylinder = models.CharField(max_length=100, null=True, blank=True)
    automated_refraction_os_axis = models.CharField(max_length=100, null=True, blank=True)
    automated_refraction_pupil_distance = models.CharField(max_length=100, null=True, blank=True)

    type_eye_glasses_od = models.CharField(max_length=100, null=True, blank=True)
    type_eye_glasses_os = models.CharField(max_length=100, null=True, blank=True)

    eye_glass_refraction_od_sphere = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_od_cylinder = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_od_axis = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_od_add = models.CharField(max_length=100, null=True, blank=True)

    eye_glass_refraction_os_sphere = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_os_cylinder = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_os_axis = models.CharField(max_length=100, null=True, blank=True)
    eye_glass_refraction_os_add = models.CharField(max_length=100, null=True, blank=True)

    for_tonometry = models.BooleanField(default=False)
    for_tonometry_eye = models.CharField(max_length=100, null=True, blank=True)
    for_tonometry_consented = models.CharField(max_length=100, null=True, blank=True)

    for_dilation = models.BooleanField(default=False)
    for_dilation_eye = models.CharField(max_length=100, null=True, blank=True)
    for_dilation_consented = models.CharField(max_length=100, null=True, blank=True)    

    intraocular_pressure_od = models.CharField(max_length=100, null=True, blank=True)
    intraocular_pressure_os = models.CharField(max_length=100, null=True, blank=True)

    dilation_od = models.CharField(max_length=100, null=True, blank=True)
    dilation_os = models.CharField(max_length=100, null=True, blank=True)

    amsler_od = models.ImageField(null=True, blank=True)
    amsler_os = models.ImageField(null=True, blank=True)

    ishihara_result = models.CharField(max_length=100, null=True, blank=True)
    ishihara_number_of_plates_correct = models.IntegerField(null=True, blank=True)
    ishihara_plates = models.IntegerField(null=True, blank=True)

    slit_lamp_od = models.ImageField(null=True, blank=True)
    slit_lamp_os = models.ImageField(null=True, blank=True)
    fundus_od = models.ImageField(null=True, blank=True)
    fundus_os = models.ImageField(null=True, blank=True)
    optic_nerve_od = models.ImageField(null=True, blank=True)
    optic_nerve_os = models.ImageField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    profile_picture = ImageField(null=True, blank=True)

    class Meta:
        pass


    def __unicode__(self):
        return u'Participant %s' % self.participant_number


class SystemicDisease(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    month_year_diagnosed = models.CharField(max_length=8, null=True, blank=True)
    duration_auto_calc = models.CharField("Duration (months)", max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Systemic Disease: %s' % self.name

class LifeStyleCondition(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    month_year_diagnosed = models.CharField(max_length=8, null=True, blank=True)
    duration_auto_calc = models.CharField("Duration (months)", max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Lifestyle Condition: %s' % self.name

class SystemicMedication(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    dose = models.CharField(max_length=100, null=True, blank=True)
    month_year_started = models.CharField(max_length=8, null=True, blank=True)
    duration_auto_calc = models.CharField("Duration (months)", max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Systemic Medication: %s' % self.name


class DietarySupplement(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    dose = models.CharField(max_length=100, null=True, blank=True)
    month_year_started = models.CharField(max_length=8, null=True, blank=True)
    duration_auto_calc = models.CharField("Duration (months)", max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Dietary Supplement: %s' % self.name


class OcularMedication(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    dose = models.CharField(max_length=100, null=True, blank=True)
    eye = models.CharField(max_length=100, null=True, blank=True)
    month_year_started = models.CharField(max_length=8, null=True, blank=True)
    duration_auto_calc = models.CharField("Duration (months)", max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Ocular Medication: %s' % self.name


class OcularSymptom(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    eye = models.CharField(max_length=100, null=True, blank=True)
    duration = models.CharField(max_length=100, null=True, blank=True)
    main_symptom_od = models.BooleanField(default=False)
    main_symptom_os = models.BooleanField(default=False)
    main_symptom_patient = models.BooleanField(default=False)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Ocular Symptom: %s' % self.name


class NonOcularSymptom(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100)
    duration = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'Non-Ocular Symptom: %s' % self.name


class AutomatedRefraction(models.Model):
    participant = models.ForeignKey(Participant)
    od_sphere = models.CharField(max_length=100, null=True, blank=True)
    od_cylinder = models.CharField(max_length=100, null=True, blank=True)
    od_axis = models.CharField(max_length=100, null=True, blank=True)
    os_sphere = models.CharField(max_length=100, null=True, blank=True)
    os_cylinder = models.CharField(max_length=100, null=True, blank=True)
    os_axis = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return u'Automated Refraction pid %s' % self.participant.id


class Household(models.Model):
    code = models.IntegerField()
    region = models.CharField(max_length=100, null=True, blank=True)
    barangay = models.CharField(max_length=100, null=True, blank=True)
    province = models.CharField(max_length=100, null=True, blank=True)
    nearest_relative = models.CharField(max_length=100, null=True, blank=True)
    nearest_relative_contact_details = models.CharField(max_length=50, null=True, blank=True)

    gps_reading = models.CharField('GPS Location Reading', max_length=50, null=True, blank=True)
    elevation = models.CharField(max_length=50, null=True, blank=True)
    distance_from_the_sea = models.CharField(max_length=100, null=True, blank=True)
    length_of_stay = models.CharField(max_length=100, null=True, blank=True)

    area = models.CharField(max_length=100, null=True, blank=True)
    locale = models.CharField(max_length=100, null=True, blank=True)
    total_household_members = models.CharField(max_length=100, null=True, blank=True)
    household_type = models.CharField(max_length=100, null=True, blank=True)
    household_head_civil_status = models.CharField(max_length=100, null=True, blank=True)
    household_head_educational_attainment = models.CharField(max_length=100, null=True, blank=True)

    number_of_cars = models.CharField(max_length=100, null=True, blank=True)
    number_of_jeep_motorcycles_tricycles = models.CharField(max_length=100, null=True, blank=True)

    fuel_types_for_cooking = models.CharField(max_length=100, null=True, blank=True)
    domestic_service = models.CharField(max_length=100, null=True, blank=True)
    laundry_service = models.CharField(max_length=100, null=True, blank=True)
    household_member_traveled_by_plane = models.CharField(max_length=100, null=True, blank=True)
    household_member_enrolled_in_private_school = models.CharField(max_length=100, null=True, blank=True)
    hired_school_transport = models.CharField(max_length=100, null=True, blank=True)
    financial_assistance_from_abroad = models.CharField(max_length=100, null=True, blank=True)
    
    stereo_cd_owned = models.CharField(max_length=100, null=True, blank=True)
    tv_owned = models.CharField(max_length=100, null=True, blank=True)
    vhs_vcd_dvd_vtr_owned = models.CharField(max_length=100, null=True, blank=True)
    ref_freezer_owned = models.CharField(max_length=100, null=True, blank=True)
    washing_machine_owned = models.CharField(max_length=100, null=True, blank=True)
    aircon_owned = models.CharField(max_length=100, null=True, blank=True)
    computer_owned = models.CharField(max_length=100, null=True, blank=True)
    phones_owned = models.CharField(max_length=100, null=True, blank=True)
    sala_sofa_owned = models.CharField(max_length=100, null=True, blank=True)

    toilet_facility = models.CharField(max_length=100, null=True, blank=True)
    main_water_source = models.CharField(max_length=100, null=True, blank=True)

    housewife_employment_status = models.CharField(max_length=100, null=True, blank=True)
    household_head_work_sector = models.CharField(max_length=100, null=True, blank=True)
    household_head_occupational_status = models.CharField(max_length=100, null=True, blank=True)

    house_ownership = models.CharField(max_length=100, null=True, blank=True)
    number_of_senior_citizens = models.CharField(max_length=100, null=True, blank=True)
    house_type = models.CharField(max_length=100, null=True, blank=True)
    roof_type = models.CharField(max_length=100, null=True, blank=True)
    outer_wall_type = models.CharField(max_length=100, null=True, blank=True)
    socio_economic_cluster = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.code


class Dropdown(models.Model):
    model_name = models.CharField(max_length=100)
    field_name = models.CharField(max_length=100)
    number_range = models.CharField(max_length=20, null=True, blank=True)
    number_range_increment = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    display_name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.display_name

    def get_options(self):
        ''' A functions that returns the dropdown options as a 2-tuple values (actual value, display string value) '''
        if not hasattr(self, 'choices'):
            self.choices = []
            self.actual_value_lookup = {}

            increment = self.number_range_increment or 1

            source_model = apps.get_model(app_label='survey', model_name=self.model_name)

            if (increment - int(increment)) == 0:
                increment = int(increment)

            if self.number_range:
                start, end = self.number_range.rsplit('-', 1)
                parser = int
                try:
                    v = int(start)
                    v = int(end)
                except ValueError, e:
                    parser = decimal.Decimal

                self.choices += [(i, str(i)) for i in np.arange(parser(start), parser(end) + increment, increment)]
            
            unordered = []
            ordered = []
            for option in self.dropdown_options.all().order_by('order', 'value'):
                if not option.order is None:
                    ordered.append(option)
                else:
                    unordered.append(option)
            self.choices += [(self.get_actual_value(source_model._meta.get_field(self.field_name).__class__.__name__, option.value), option.value) for option in unordered]

            for option in ordered:
                self.choices.insert(option.order, (option.value, option.value))

            self.choices.insert(0, (None, ''))

            for choice in self.choices:
                self.actual_value_lookup[choice[1]] = choice[0]
        return self.choices


    def get_actual_value(self, fieldType, value):
        actual = None
        if fieldType == 'CharField':
            actual = value
        elif fieldType == 'IntegerField':
            actual = int(value)
        elif fieldType == 'DecimalField':
            actual = decimal.DecimalField(value)
        return actual


    def get_dropdown_option_actual_value(value):
        return self.actual_value_lookup[value]


    class Meta:
        verbose_name = 'Variable'


class DropdownOption(models.Model):
    dropdown = models.ForeignKey(Dropdown, related_name='dropdown_options')
    value = models.CharField(max_length=200)
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.dropdown, self.value)


class OcularFindingDiagnosis(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100, null=True, blank=True)
    eye = models.CharField(max_length=100, null=True, blank=True)
    main_diagnosis_od = models.BooleanField("Main diagnosis OD", default=False)
    main_diagnosis_os = models.BooleanField("Main diagnosis OS", default=False)
    main_diagnosis_patient = models.BooleanField(default=False)
    main_cause_visual_loss = models.BooleanField(default=False)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'%s' % self.name


class OcularProcedure(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100, null=True, blank=True)
    eye = models.CharField(max_length=100, null=True, blank=True)
    surgery_outcome = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'%s' % self.name


class Management(models.Model):
    participant = models.ForeignKey(Participant)
    name = models.CharField(max_length=100, null=True, blank=True)
    eye = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'%s' % self.name


class AlcoholicBeverage(models.Model):
    participant = models.ForeignKey(Participant)
    alcoholic_type = models.CharField(max_length=100, null=True, blank=True)
    number_of_glasses = models.CharField(max_length=100, null=True, blank=True)
    drinking_frequency = models.CharField(max_length=100, null=True, blank=True)
    drinking_duration = models.CharField(max_length=100, null=True, blank=True)
    drinking_total_glasses = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ['id', ]

    def __unicode__(self):
        return u'%s' % self.alcoholic_type
