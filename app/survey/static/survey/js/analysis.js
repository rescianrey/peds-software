$(function() {
	$('table#conditions-table tbody tr').each(function(){
		filterAssistant($(this));
	});

	$('.pagination-buttons').click(function(e){
		$('input[name="page"]').val($(this).data('page'));
		$('#submit').click();
		return false;
	});
});

