# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-10 15:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0047_participant_age'),
    ]

    operations = [
        migrations.RenameField(
            model_name='participant',
            old_name='gender',
            new_name='sex',
        ),
    ]
