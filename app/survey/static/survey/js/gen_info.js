$(function(){
	var household_code_field_id = '#id_code';

	var generateCodeBtn = $('<button class="btn btn-primary">Generate new household code</button>');
	$(household_code_field_id).parent().after(generateCodeBtn);

	generateCodeBtn.click(function(){
		$.get(generateHouseholdURL, function(data, status, xhr){
			if(status == 'success'){
				$(household_code_field_id).val(data);
			}
		});
		return false;
	});

	$('#id_birthday').change(function(){
		if( $(this).val() != '' ){
			var bdate = new Date($(this).val());


			var ageDifMs = Date.now() - bdate.getTime();
    		var ageDate = new Date(ageDifMs); // miliseconds from epoch
    		var ageInYears = Math.abs(ageDate.getUTCFullYear() - 1970);

    		$('#id_age').val(ageInYears);
		}
	});

	$(household_code_field_id).change(function(){
		var url = retrieveHouseholdURL + '?code=' + $(this).val() + '&fields=province,barangay,region,nearest_relative,nearest_relative_contact_details'
		$.get(url,
			function(data, status, xhr){
				if(status == 'success'){
					setValue($('#id_region'), data['region']);
					setValue($('#id_barangay'), data['barangay']);
					setValue($('#id_province'), data['province']);
					setValue($('#id_nearest_relative'), data['nearest_relative']);
					setValue($('#id_nearest_relative_contact_details'), data['nearest_relative_contact_details']);
				}
			},
			'json'
		);
		return false;
	});

	$('#printable-btn').click(function(){
		window.open(printableURL);
	});

	$('#id_age').attr('readonly', '');
	$('#id_date_started').attr('readonly', '');
});
