# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-06 16:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0016_auto_20160206_1632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='study',
            name='is_dependent_of_insurance',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
