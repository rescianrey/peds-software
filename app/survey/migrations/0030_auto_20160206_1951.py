# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-06 19:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0029_auto_20160206_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='area_classification',
            field=models.IntegerField(blank=True, choices=[(1, 'Urban'), (2, 'Rural')], null=True),
        ),
        migrations.AddField(
            model_name='study',
            name='distance_from_the_sea',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True),
        ),
        migrations.AddField(
            model_name='study',
            name='elevation',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='study',
            name='gps_reading',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
