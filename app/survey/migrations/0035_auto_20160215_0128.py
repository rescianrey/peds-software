# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-15 01:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0034_participant'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Study',
        ),
        migrations.AlterModelOptions(
            name='participant',
            options={},
        ),
    ]
