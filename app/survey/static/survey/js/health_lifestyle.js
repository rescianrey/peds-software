$(function(){
    $('#div_id_number_of_sticks').removeAttr('class');
    $('#div_id_number_of_sticks .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_smoking_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_smoking_duration .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_recreational_drug_duration .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_pack_years .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_alcoholic_type .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_number_of_bottles .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_drinking_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_recreational_drug_type .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_type_of_smoke .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_smoker .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_alcoholic_beverages .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_recreational_drug .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_rice_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_beef_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_pork_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_chicken_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_egg_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_fish_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_vegetables_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('#div_id_junkfoods_plate_proportion .controls').removeClass('col-sm-5').addClass('col-sm-2');
    $('th[for="id_ocularmedication_set-0-dose"]').addClass('col-sm-2');
    $('th[for="id_ocularmedication_set-0-eye"]').addClass('col-sm-2');
    $('th[for="id_systemicmedication_set-0-dose"]').addClass('col-sm-2');
    $('th[for="id_ocularmedication_set-0-name"]').addClass('col-sm-2');

    $('th[for="id_dietarysupplement_set-0-dose"]').addClass('col-sm-2');
    $('th[for="id_alcoholicbeverage_set-0-alcoholic_type"]').addClass('col-sm-3');
    $('th[for="id_alcoholicbeverage_set-0-drinking_frequency"]').addClass('col-sm-3');
    $('th[for="id_alcoholicbeverage_set-0-drinking_total_glasses"]').addClass('col-sm-2');

    $('#div_id_slices_of_bread').removeAttr('class');
    $('#div_id_slices_of_bread .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_bread_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_bread_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_glasses_of_milk').removeAttr('class');
    $('#div_id_glasses_of_milk .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_milk_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_milk_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_glasses_of_fruit_juice').removeAttr('class');
    $('#div_id_glasses_of_fruit_juice .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_fruit_juice_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');
    $('#div_id_fruit_juice_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_fruit_juice_intake_frequency .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('#div_id_fruit_juice_type .controls').removeClass('col-sm-5').addClass('col-sm-3');

    $('input[id$="drinking_total_glasses"').attr('readonly', '');

    showDivIfFieldEquals('smoke-details', 'id_smoker', ['Yes, current smoker', 'Yes, past smoker']);
    $('#id_smoker').change(function(){
        showDivIfFieldEquals('smoke-details', 'id_smoker', ['Yes, current smoker', 'Yes, past smoker']);
    });

    showDivIfFieldEquals('alcohol-consumption-details', 'id_alcoholic_beverages', ['Yes, Occassionally', 'Yes, Regularly']);
    $('#id_alcoholic_beverages').change(function(){
        showDivIfFieldEquals('alcohol-consumption-details', 'id_alcoholic_beverages', ['Yes, Occassionally', 'Yes, Regularly']);
    });

    showDivIfFieldEquals('recreational-drug-details', 'id_recreational_drug', ['Yes, current user', 'Yes, past user']);
    $('#id_recreational_drug').change(function(){
        showDivIfFieldEquals('recreational-drug-details', 'id_recreational_drug', ['Yes, current user', 'Yes, past user']);
    });

    showDivIfFieldEquals('glasses-details', 'id_use_protective_glasses', 'Yes');
    $('#id_use_protective_glasses').change(function(){
        showDivIfFieldEquals('glasses-details', 'id_use_protective_glasses', 'Yes');
    });

    showDivIfFieldEquals('hat-umbrella-details', 'id_use_hat_or_umbrella', 'Yes');
    $('#id_use_hat_or_umbrella').change(function(){
        showDivIfFieldEquals('hat-umbrella-details', 'id_use_hat_or_umbrella', 'Yes');
    });

    showDivIfFieldEquals('menopausal-details', 'id_has_menstrual_period_ended', 'Yes');
    $('#id_has_menstrual_period_ended').change(function(){
        showDivIfFieldEquals('menopausal-details', 'id_has_menstrual_period_ended', 'Yes');
    });

    $('input[id*="_total_average_"]').attr('readonly', '');

    // Calculate BMI
    $('#id_height').change(function(){
        calculateBMI();
    });

    $('#id_weight').change(function(){
        calculateBMI();
    });

    // Calculate Pack years
    $('#id_number_of_sticks').change(function(){
        calculatePackYears();
    });

    $('#id_smoking_duration').change(function(){
        calculatePackYears();
    });

    $('#id_smoking_frequency').change(function(){
        calculatePackYears();
    });

    $('#id_number_of_years_elementary_sunlight_exposure, #id_number_of_years_highschool_sunlight_exposure, #id_number_of_years_college_sunlight_exposure, #id_number_of_years_working_sunlight_exposure, #id_number_of_years_retired_sunlight_exposure').change(function(){
        calculateTotal([$('#id_number_of_years_elementary_sunlight_exposure'), 
            $('#id_number_of_years_highschool_sunlight_exposure'),
            $('#id_number_of_years_college_sunlight_exposure'),
            $('#id_number_of_years_working_sunlight_exposure'),
            $('#id_number_of_years_retired_sunlight_exposure')],
            null,
            null,
            $('#id_number_of_years_total_average_sunlight_exposure'));
    });
    
    $('#id_number_of_years_elementary_sunlight_exposure, #id_number_of_years_highschool_sunlight_exposure, #id_number_of_years_college_sunlight_exposure, #id_number_of_years_working_sunlight_exposure, #id_number_of_years_retired_sunlight_exposure, #id_number_of_hours_spent_elementary_sunlight_exposure, #id_number_of_hours_spent_highschool_sunlight_exposure, #id_number_of_hours_spent_college_sunlight_exposure, #id_number_of_hours_spent_working_sunlight_exposure, #id_number_of_hours_spent_retired_sunlight_exposure').change(function(){
        calculateTotal([$('#id_number_of_years_elementary_sunlight_exposure'), 
            $('#id_number_of_years_highschool_sunlight_exposure'),
            $('#id_number_of_years_college_sunlight_exposure'),
            $('#id_number_of_years_working_sunlight_exposure'),
            $('#id_number_of_years_retired_sunlight_exposure')],

            [$('#id_number_of_hours_spent_elementary_sunlight_exposure'), 
            $('#id_number_of_hours_spent_highschool_sunlight_exposure'),
            $('#id_number_of_hours_spent_college_sunlight_exposure'),
            $('#id_number_of_hours_spent_working_sunlight_exposure'),
            $('#id_number_of_hours_spent_retired_sunlight_exposure')], 

            $('#id_number_of_years_total_average_sunlight_exposure'),
            $('#id_number_of_hours_spent_total_average_sunlight_exposure'));
    });
    
    $('#id_number_of_years_elementary_near_work, #id_number_of_years_highschool_near_work, #id_number_of_years_college_near_work, #id_number_of_years_working_near_work, #id_number_of_years_retired_near_work').change(function(){
        calculateTotal([$('#id_number_of_years_elementary_near_work'), 
            $('#id_number_of_years_highschool_near_work'),
            $('#id_number_of_years_college_near_work'),
            $('#id_number_of_years_working_near_work'),
            $('#id_number_of_years_retired_near_work')], null, null, $('#id_number_of_years_total_average_near_work'));
    });

    $('#id_number_of_years_elementary_near_work, #id_number_of_years_highschool_near_work, #id_number_of_years_college_near_work, #id_number_of_years_working_near_work, #id_number_of_years_retired_near_work, #id_number_of_hours_spent_elementary_near_work, #id_number_of_hours_spent_highschool_near_work, #id_number_of_hours_spent_college_near_work, #id_number_of_hours_spent_working_near_work, #id_number_of_hours_spent_retired_near_work').change(function(){
        calculateTotal([$('#id_number_of_years_elementary_near_work'), 
            $('#id_number_of_years_highschool_near_work'),
            $('#id_number_of_years_college_near_work'),
            $('#id_number_of_years_working_near_work'),
            $('#id_number_of_years_retired_near_work')],

            [$('#id_number_of_hours_spent_elementary_near_work'), 
            $('#id_number_of_hours_spent_highschool_near_work'),
            $('#id_number_of_hours_spent_college_near_work'),
            $('#id_number_of_hours_spent_working_near_work'),
            $('#id_number_of_hours_spent_retired_near_work')], 

            $('#id_number_of_years_total_average_near_work'),
            $('#id_number_of_hours_spent_total_average_near_work'));
    });

    $('#id_number_of_years_elementary_near_work, #id_number_of_years_highschool_near_work, #id_number_of_years_college_near_work, #id_number_of_years_working_near_work, #id_number_of_years_retired_near_work, #id_number_of_hours_spent_near_computer_elementary_near_work, #id_number_of_hours_spent_near_computer_highschool_near_work, #id_number_of_hours_spent_near_computer_college_near_work, #id_number_of_hours_spent_near_computer_working_near_work, #id_number_of_hours_spent_near_computer_retired_near_work').change(function(){
        calculateTotal([$('#id_number_of_years_elementary_near_work'), 
            $('#id_number_of_years_highschool_near_work'),
            $('#id_number_of_years_college_near_work'),
            $('#id_number_of_years_working_near_work'),
            $('#id_number_of_years_retired_near_work')],

            [$('#id_number_of_hours_spent_near_computer_elementary_near_work'), 
            $('#id_number_of_hours_spent_near_computer_highschool_near_work'),
            $('#id_number_of_hours_spent_near_computer_college_near_work'),
            $('#id_number_of_hours_spent_near_computer_working_near_work'),
            $('#id_number_of_hours_spent_near_computer_retired_near_work')], 

            $('#id_number_of_years_total_average_near_work'),
            $('#id_number_of_hours_spent_near_computer_total_average_near_work'));
    });


    syncFields('id_number_of_years_elementary_sunlight_exposure', 'id_number_of_years_elementary_near_work');
    syncFields('id_number_of_years_highschool_sunlight_exposure', 'id_number_of_years_highschool_near_work');
    syncFields('id_number_of_years_college_sunlight_exposure', 'id_number_of_years_college_near_work');
    syncFields('id_number_of_years_working_sunlight_exposure', 'id_number_of_years_working_near_work');
    syncFields('id_number_of_years_retired_sunlight_exposure', 'id_number_of_years_retired_near_work');

    $('#id_pack_years').attr('readonly', '');

    $('input[id$="drinking_total_glasses"]').change(function(){
        console.log("fef");
    });
});

function calculateBMI(){
    if($('#id_height').val() != '' && $('#id_weight').val() != ''){
        var weight = parseInt($('#id_weight').val());
        var height = parseInt($('#id_height').val());
        var bmi = weight / Math.pow(height/100, 2);
        $('#id_bmi').val(bmi.toFixed(2));
        classifyBMI();
    }
}

function classifyBMI(){
    var value = '';
    if($('#id_bmi').val() != ''){
        var bmi = parseFloat($('#id_bmi').val());
        if(bmi < 16.0){
            value = 'Severe Underweight';
        }else if(bmi >= 16.0 && bmi < 17){
            value = 'Moderate Underweight';
        }else if(bmi >= 17 && bmi < 18.5){
            value = 'Mild Underweight';
        }else if(bmi >= 18.5 && bmi < 25){
            value = 'Normal';
        }else if(bmi >= 25.0 && bmi < 30.0){
            value = 'Pre Obese';
        }else if(bmi >= 30.0 && bmi < 35.0){
            value = 'Obese - Class I';
        }else if(bmi >= 35.0 && bmi < 40){
            value = 'Obese - Class II';
        }else{
            value = 'Obese - Class III';
        }
    }
    $('#id_classification').val(value);
}

function calculatePackYears(){
    var numberOfSticks = $('#id_number_of_sticks').val();
    var yearsSmoking = $('#id_smoking_duration').val();
    var frequency = $('#id_smoking_frequency').val();

    if(numberOfSticks != '' && yearsSmoking != '' && frequency != ''){
        var numberOfSticks_int = parseInt(numberOfSticks);
        var yearsSmoking_int = parseInt(yearsSmoking);
        var result = 0.0;

        if(frequency == 'Daily'){
            result = (numberOfSticks_int/20) * yearsSmoking_int;
        }else if(frequency == 'Monthly'){
            result = (numberOfSticks_int/(20*30)) * yearsSmoking_int;
        }else{
            result = (numberOfSticks_int/(20*7)) * yearsSmoking_int;
        }

        $('#id_pack_years').val(result.toFixed(2));
    }
}